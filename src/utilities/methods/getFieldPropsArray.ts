import { GetFieldPropsArrayProps } from '../../types/method';
import { FieldProps } from '../../types/components/Board';
import getFieldProps from './getFieldProps';

const getFieldPropsArray = ({
	start,
	end,
	town,
}: GetFieldPropsArrayProps): FieldProps[] => {
	const fields = [];
	for (let number = start; number <= end; number++) {
		fields.push(getFieldProps(number, town));
	}
	return fields;
};

export default getFieldPropsArray;
