const getArrayWithPageNumbers = (count: number): number[] => Array.from(Array(count).keys());

export default getArrayWithPageNumbers;