import { GetTaxByFieldNumberProps } from '../../types/method';
import { Tax } from '../../types/tax';

const getTaxByFieldNumber = ({
	number,
	town,
}: GetTaxByFieldNumberProps): Tax | undefined => town.taxes
	.find(tax => tax.number === number);

export default getTaxByFieldNumber;