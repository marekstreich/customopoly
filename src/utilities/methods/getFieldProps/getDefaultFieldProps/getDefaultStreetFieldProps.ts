import { FieldType, StreetFieldNumber } from '../../../../types/field';
import { DistrictColor } from '../../../../types/district';
import { StreetFieldProps } from '../../../../types/components/Board';

const getDefaultStreetFieldProps = (number: StreetFieldNumber): StreetFieldProps => ({
	type: FieldType.street,
	name: '',
	color: DistrictColor.yellow,
	number,
	price: 0,
});

export default getDefaultStreetFieldProps;