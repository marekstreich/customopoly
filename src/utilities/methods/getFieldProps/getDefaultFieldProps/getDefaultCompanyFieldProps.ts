import { FieldType, CompanyFieldNumber } from '../../../../types/field';
import { CompanyFieldProps } from '../../../../types/components/Board';
import { CompanyIcon } from '../../../../types/company';

const getDefaultCompanyFieldProps = (number: CompanyFieldNumber): CompanyFieldProps => ({
	type: FieldType.company,
	name: '',
	icon: CompanyIcon.industry,
	number,
});

export default getDefaultCompanyFieldProps;