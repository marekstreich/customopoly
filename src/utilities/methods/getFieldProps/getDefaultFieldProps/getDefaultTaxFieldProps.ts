import { FieldType, TaxFieldNumber } from '../../../../types/field';
import { TaxFieldProps } from '../../../../types/components/Board';
import { TaxIcon } from '../../../../types/tax';

const getDefaultTaxFieldProps = (number: TaxFieldNumber): TaxFieldProps => ({
	type: FieldType.tax,
	name: '',
	icon: TaxIcon.dollar,
	number,
});

export default getDefaultTaxFieldProps;