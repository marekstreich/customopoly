import { FieldType, StationFieldNumber } from '../../../../types/field';
import { StationFieldProps } from '../../../../types/components/Board';

const getDefaultStationFieldProps = (number: StationFieldNumber): StationFieldProps => ({
	type: FieldType.station,
	name: '',
	number,
});

export default getDefaultStationFieldProps;