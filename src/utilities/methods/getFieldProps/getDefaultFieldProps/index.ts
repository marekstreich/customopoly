export { default as getDefaultCompanyFieldProps } from './getDefaultCompanyFieldProps';
export { default as getDefaultStationFieldProps } from './getDefaultStationFieldProps';
export { default as getDefaultStreetFieldProps } from './getDefaultStreetFieldProps';
export { default as getDefaultTaxFieldProps } from './getDefaultTaxFieldProps';