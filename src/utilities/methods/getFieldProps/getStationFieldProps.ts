import { FieldType, StationFieldNumber } from '../../../types/field';
import { Town } from '../../../types/town';
import { StationFieldProps } from '../../../types/components/Board';
import { getStationByFieldNumber } from '../index';
import { getDefaultStationFieldProps } from './getDefaultFieldProps';

const getStationFieldProps = (number: StationFieldNumber, town: Town): StationFieldProps => {
	const station = getStationByFieldNumber({ number, town });
	if (!station) return getDefaultStationFieldProps(number);

	return {
		type: FieldType.station,
		name: station.name,
		number,
	};
};

export default getStationFieldProps;