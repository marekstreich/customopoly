import {
	FieldType,
	SideFieldNumber,
	ChanceFieldNumber,
	CompanyFieldNumber,
	StationFieldNumber,
	StreetFieldNumber,
	TaxFieldNumber,
} from '../../../types/field';
import { Town } from '../../../types/town';
import { FieldProps } from '../../../types/components/Board';
import getFieldTypeByFieldNumber from '../getFieldTypeByFieldNumber';
import getChanceFieldProps from './getChanceFieldProps';
import getCompanyFieldProps from './getCompanyFieldProps';
import getStationFieldProps from './getStationFieldProps';
import getStreetFieldProps from './getStreetFieldProps';
import getTaxFieldProps from './getTaxFieldProps';

const getFieldProps = (number: SideFieldNumber, town: Town): FieldProps => {
	switch (getFieldTypeByFieldNumber(number)) {
		case FieldType.chance:
			return getChanceFieldProps(number as ChanceFieldNumber);
		case FieldType.company:
			return getCompanyFieldProps(number as CompanyFieldNumber, town);
		case FieldType.station:
			return getStationFieldProps(number as StationFieldNumber, town);
		case FieldType.street:
			return getStreetFieldProps(number as StreetFieldNumber, town);
		case FieldType.tax:
			return getTaxFieldProps(number as TaxFieldNumber, town);
		default:
			return getChanceFieldProps(number as ChanceFieldNumber);
	}
};

export default getFieldProps;
