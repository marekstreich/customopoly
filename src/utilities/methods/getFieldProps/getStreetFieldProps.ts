import { FieldType, StreetFieldNumber } from '../../../types/field';
import { Town } from '../../../types/town';
import { StreetFieldProps } from '../../../types/components/Board';
import { getStreetWithDistrictByFieldNumber, getPriceByColorAndStreetType } from '../index';
import { getDefaultStreetFieldProps } from './getDefaultFieldProps';

const getStreetFieldProps = (number: StreetFieldNumber, town: Town): StreetFieldProps => {
	const street = getStreetWithDistrictByFieldNumber({ number, town });
	if (!street) return getDefaultStreetFieldProps(number);

	const price = getPriceByColorAndStreetType({
		color: street.color,
		type: street.type,
		town,
	});

	return {
		type: FieldType.street,
		name: street.name,
		color: street.color,
		number,
		price,
	};
};

export default getStreetFieldProps;