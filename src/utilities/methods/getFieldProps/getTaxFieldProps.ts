import { FieldType, TaxFieldNumber } from '../../../types/field';
import { Town } from '../../../types/town';
import { TaxFieldProps } from '../../../types/components/Board';
import { getTaxByFieldNumber } from '../index';
import { getDefaultTaxFieldProps } from './getDefaultFieldProps';

const getTaxFieldProps = (number: TaxFieldNumber, town: Town): TaxFieldProps => {
	const tax = getTaxByFieldNumber({ number, town });
	if (!tax) return getDefaultTaxFieldProps(number);

	return {
		type: FieldType.tax,
		name: tax.name,
		icon: tax.icon,
		number,
	};
};

export default getTaxFieldProps;