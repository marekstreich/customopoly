import { FieldType, ChanceFieldNumber } from '../../../types/field';
import { ChanceFieldProps } from '../../../types/components/Board';

const getChanceFieldProps = (number: ChanceFieldNumber): ChanceFieldProps => ({
	type: FieldType.chance,
	number,
});

export default getChanceFieldProps;