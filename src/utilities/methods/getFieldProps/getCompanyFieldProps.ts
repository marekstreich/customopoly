import { FieldType, CompanyFieldNumber } from '../../../types/field';
import { Town } from '../../../types/town';
import { CompanyFieldProps } from '../../../types/components/Board';
import { getCompanyByFieldNumber } from '../index';
import { getDefaultCompanyFieldProps } from './getDefaultFieldProps';

const getCompanyFieldProps = (number: CompanyFieldNumber, town: Town): CompanyFieldProps => {
	const company = getCompanyByFieldNumber({ number, town });
	if (!company) return getDefaultCompanyFieldProps(number);

	return {
		type: FieldType.company,
		name: company.name,
		icon: company.icon,
		number,
	};
};

export default getCompanyFieldProps;