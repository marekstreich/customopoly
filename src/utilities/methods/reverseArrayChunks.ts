const reverseArrayChunks = <T>(input: T[], chunkLength: number): T[] => {
	const chunks = [];
	for (let index = 0; index < input.length; index += chunkLength) {
        chunks.push(input.slice(index, index + chunkLength).reverse());
	}
	return chunks.flat();
};

export default reverseArrayChunks;
