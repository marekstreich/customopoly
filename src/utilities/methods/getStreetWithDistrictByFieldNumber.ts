import { GetStreetWithDistrictByFieldNumberProps } from '../../types/method';
import { StreetWithDistrict } from '../../types/intersection';
import getStreetWithDistrict from './getStreetWithDistrict';

const getStreetWithDistrictByFieldNumber = ({
	number,
	town,
}: GetStreetWithDistrictByFieldNumberProps): StreetWithDistrict | undefined => {
	const district = town.districts
		.find((district) => district.streets
			.find((street) => street.number === number));

	if (!district) return;
	const street = district.streets
		.find((street) => street.number === number);

	if (!street) return;
	return getStreetWithDistrict({ street, district });
};

export default getStreetWithDistrictByFieldNumber;