import {
	FIELD_TYPE_MAP,
	FieldType,
	SideFieldNumber,
} from '../../types/field';

const getFieldTypeByFieldNumber = (number: SideFieldNumber): FieldType => {
	let type = FieldType.tax;
	Object.entries(FIELD_TYPE_MAP).forEach(([fieldType, fieldNumbers]) => {
		if (fieldNumbers.includes(number)) type = fieldType as FieldType;
	});
	return type;
};

export default getFieldTypeByFieldNumber;