import { GetStationByFieldNumberProps } from '../../types/method';
import { Station } from '../../types/station';

const getStationByFieldNumber = ({
	number,
	town,
}: GetStationByFieldNumberProps): Station | undefined => town.stations
	.find(station => station.number === number);

export default getStationByFieldNumber;