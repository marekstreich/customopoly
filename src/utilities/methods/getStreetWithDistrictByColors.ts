import { StreetWithDistrict } from '../../types/intersection';
import { GetStreetWithDistrictByColorsProps } from '../../types/method';
import { getStreetWithDistrict, getDistrictsByColors, reverseArrayChunks } from './index';

const getStreetWithDistrictByColors = ({
	town,
	colors,
	reverseChunkLength = 0,
}: GetStreetWithDistrictByColorsProps): StreetWithDistrict[] => {
	const districts = getDistrictsByColors({ town, colors });
	const streets: StreetWithDistrict[] = [];
	districts
		.forEach((district) => district.streets
			.forEach((street) => streets.push(getStreetWithDistrict({ street, district }))));
	return reverseChunkLength
		? reverseArrayChunks(streets, reverseChunkLength)
		: streets;
};

export default getStreetWithDistrictByColors;