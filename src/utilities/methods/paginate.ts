const paginate = <T>(array: T[], pageSize: number, pageNumber: number): T[] =>
	array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);

export default paginate;