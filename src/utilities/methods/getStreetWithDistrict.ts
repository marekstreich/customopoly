import { GetStreetWithDistrictProps } from "../../types/method";
import { StreetWithDistrict } from "../../types/intersection";

const getStreetWithDistrict = ({ street, district }: GetStreetWithDistrictProps): StreetWithDistrict => ({
	name: street.name,
	type: street.type,
	number: street.number,
	district: district.name,
	color: district.color,
});

export default getStreetWithDistrict;