import { GetCompanyByFieldNumberProps } from '../../types/method';
import { Company } from '../../types/company';

const getCompanyByFieldNumber = ({
	number,
	town,
}: GetCompanyByFieldNumberProps): Company | undefined => town.companies
	.find(company => company.number === number);

export default getCompanyByFieldNumber;