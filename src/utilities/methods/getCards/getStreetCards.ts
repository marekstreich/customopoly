import { GetStreetCardsProps, GetStreetCardsReturnType } from '../../../types/method';
import getStreetWithDistrict from '../getStreetWithDistrict';

const getStreetCards = ({
	districts,
}: GetStreetCardsProps): GetStreetCardsReturnType => {
	return districts
		.map((district) => district.streets.map(
			(street) => getStreetWithDistrict({ street, district })
		))
		.flat();
};

export default getStreetCards;
