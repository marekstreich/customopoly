import { ChanceType } from '../../../types/chance';
import { GetChanceCardsProps, GetChanceCardsReturnType, ChanceCard } from '../../../types/method';

const getChanceCards = ({
	chances,
}: GetChanceCardsProps): GetChanceCardsReturnType => {
	return Object.entries(chances).reduce((obj, [type, set], setIndex) => {
		(obj as GetChanceCardsReturnType)[type as ChanceType] = Object
			.values(set)
			.map((subset) => Object.values(subset as object))
			.flat()
			.map((text, textIndex) => ({
				text,
				key: `chance-${setIndex + 1}-${textIndex + 1}`,
			})) as ChanceCard[];
		return obj;
	}, {});
};

export default getChanceCards;
