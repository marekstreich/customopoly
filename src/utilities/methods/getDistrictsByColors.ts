import { PrintablePagesWithCardsByColorsProps } from '../../types/components/PrintablePage';
import { District } from '../../types/district';

const getDistrictsByColors = ({
	town,
	colors,
}: PrintablePagesWithCardsByColorsProps): District[] => town
	.districts
	.filter(({ color }) => colors.includes(color));

export default getDistrictsByColors;