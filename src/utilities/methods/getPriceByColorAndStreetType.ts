import { GetPriceByColorAndStreetTypeProps } from '../../types/method';

const getPriceByColorAndStreetType = ({
	color,
	type,
	town,
}: GetPriceByColorAndStreetTypeProps): number => town.price[color][type];

export default getPriceByColorAndStreetType;