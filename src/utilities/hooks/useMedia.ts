import { useState, useEffect, useRef, useCallback } from 'react';
import { mobile, tablet, desktop, aboveMobile, belowDesktop } from '../styles/medias';

const useMatchMediaListener = (query: string) => {
	const mediaRef = useRef<MediaQueryList | null>(null);
	const [mediaMatches, setMediaMatches] = useState<boolean | null>(null);
	const listener = useCallback((event) => {
		setMediaMatches(event.matches);
	}, []);

	useEffect(() => {
		mediaRef.current = window.matchMedia(query);
		mediaRef.current.addListener(listener);
		setMediaMatches(mediaRef.current.matches);
		return () => {
			mediaRef?.current?.removeListener(listener);
		};
	}, [listener, query]);

	return mediaMatches;
}

export const useIsMobile = () => useMatchMediaListener(mobile);
export const useIsTablet = () => useMatchMediaListener(tablet);
export const useIsDesktop = () => useMatchMediaListener(desktop);
export const useIsAboveMobile = () => useMatchMediaListener(aboveMobile);
export const useIsBelowDesktop = () => useMatchMediaListener(belowDesktop);