export {
	useIsMobile,
	useIsTablet,
	useIsDesktop,
	useIsAboveMobile,
	useIsBelowDesktop,
} from './useMedia';
