const fontFamilies = {
	dosis: `'Dosis', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Helvetica Neue', sans-serif`,
	nunito: `'Nunito', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Helvetica Neue', sans-serif`,
};

export default fontFamilies;