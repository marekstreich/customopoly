export const mobile = '(max-width: 767.98px)';
export const tablet = '(min-width: 768px) and (max-width: 1023.98px)';
export const desktop = '(min-width: 1024px)';
export const aboveMobile = '(min-width: 768px)';
export const belowDesktop = '(max-width: 1023.98px)';
