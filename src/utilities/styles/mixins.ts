export const shadowPrintFix = `
	-webkit-print-color-adjust: exact;
	-webkit-filter: opacity(1);
	filter: opacity(1);
`;
