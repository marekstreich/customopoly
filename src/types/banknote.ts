import { colors } from "../utilities/styles";

export type BanknoteValue = 5 | 10 | 20 | 50 | 100 | 500 | 1000;

export type BanknoteValueColorMap = Record<BanknoteValue, string>;

export const BANKNOTE_VALUE_COLOR_MAP: BanknoteValueColorMap = {
	5: colors.GAME.yellow,
	10: colors.GAME.orange,
	20: colors.GAME.blue,
	50: colors.GAME.pink,
	100: colors.GAME.green,
	500: colors.GAME.red,
	1000: colors.GAME.black,
};
