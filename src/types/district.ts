import { Street } from './street';

export enum DistrictColor {
	yellow = 'yellow',
	orange = 'orange',
	red = 'red',
	pink = 'pink',
	purple = 'purple',
	blue = 'blue',
	teal = 'teal',
	green = 'green',
}

export type District = {
	name: string;
	color: DistrictColor;
	streets: Street[];
}

export const districtOrder: DistrictColor[] = [
	DistrictColor.yellow,
	DistrictColor.orange,
	DistrictColor.red,
	DistrictColor.pink,
	DistrictColor.purple,
	DistrictColor.blue,
	DistrictColor.teal,
	DistrictColor.green,
];
