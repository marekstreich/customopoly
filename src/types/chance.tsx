import React, { ReactNode } from 'react';
import { FaStar, FaDice } from 'react-icons/fa';
import { ChanceFieldNumber } from "./field";
import { colors } from "../utilities/styles";

export enum ChanceType {
	red = 'red',
	blue = 'blue',
};

export type ChanceTypeNumberMap = Record<ChanceFieldNumber, ChanceType>;

export const CHANCE_TYPE_NUMBER_MAP: ChanceTypeNumberMap = {
	3: ChanceType.blue,
	8: ChanceType.red,
	18: ChanceType.blue,
	23: ChanceType.red,
	34: ChanceType.blue,
	37: ChanceType.red,
};

export type ChanceTypeColorMap = Record<ChanceType, string>;

export const CHANE_TYPE_COLOR_MAP: ChanceTypeColorMap = {
	[ChanceType.red]: colors.GAME.red,
	[ChanceType.blue]: colors.GAME.blue,
};

export type ChanceTypeIconMap = Record<ChanceType, ReactNode>;

export const CHANCE_TYPE_ICON_MAP: ChanceTypeIconMap = {
	[ChanceType.red]: <FaStar />,
	[ChanceType.blue]: <FaDice />,
};

export type ChanceCashFlowOnceSet = {
	once20: string;
	once30: string;
	once50: string;
	once100: string;
	once200: string;
	once300: string;
	once400: string;
};

export type ChanceEarnSet = ChanceCashFlowOnceSet & {
	getFromEachPlayer: string;
};

export type ChancePaySet = ChanceCashFlowOnceSet & {
	repairHouses: string;
};

export type ChanceCustomSet = {
	custom1?: string;
	custom2?: string;
	custom3?: string;
	custom4?: string;
	custom5?: string;
	custom6?: string;
	custom7?: string;
	custom8?: string;
};

export type ChanceGoSet = {
	goToStart: string;
	backToStart: string;
	goToJail: string;
};

export type ChanceGoRedSet = ChanceGoSet & {
	go3Fields: string;
	back2Fields: string;
	goToYellowDistrict: string;
	goToPinkDistrict: string;
	goToPurpleDistrict: string;
	goToGreenDistrict: string;
	goToStation1: string;
	goToStation4: string;
	goToRedCompany: string;
};

export type ChanceGoBlueSet = ChanceGoSet & {
	go2Fields: string;
	back3Fields: string;
	goToOrangeDistrict: string;
	goToRedDistrict: string;
	goToBlueDistrict: string;
	goToTealDistrict: string;
	goToStation2: string;
	goToStation3: string;
	goToBlueCompany: string;
};

export type ChanceCommonSet = {
	earn: ChanceEarnSet;
	pay: ChancePaySet;
	custom?: ChanceCustomSet;
}

export type ChanceRedSet = ChanceCommonSet & {
	go: ChanceGoRedSet;
};

export type ChanceBlueSet = ChanceCommonSet & {
	go: ChanceGoBlueSet;
};

export type ChanceSet = {
	[ChanceType.red]: ChanceRedSet;
	[ChanceType.blue]: ChanceBlueSet,
};
