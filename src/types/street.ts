import { StreetFieldNumber } from './field';

export enum StreetType {
	regular = 'regular',
	main = 'main',
}

export type Street = {
	name: string;
	number: StreetFieldNumber;
	type: StreetType;
}