import { District, DistrictColor } from './district';
import { ChanceSet } from './chance';
import { Company } from './company';
import { Station } from './station';
import { DistrictCharge } from './charge';
import { DistrictPrice } from './price';
import { Tax } from './tax';

export type TownCharge = Record<DistrictColor, DistrictCharge>;
export type TownPrice = Record<DistrictColor, DistrictPrice>;
export type TownMeta = {
	title?: string;
	subtitle?: string;
	creator: string;
};

export type Town = {
	name: string;
	meta: TownMeta;
	districts: District[];
	companies: Company[];
	stations: Station[];
	charge: TownCharge;
	price: TownPrice;
	taxes: Tax[];
	chances: ChanceSet;
}
