import { StreetType } from './street';

export enum ChargeTypes {
	empty = 'empty',
	house1 = 'house1',
	house2 = 'house2',
	house3 = 'house3',
	house4 = 'house4',
	hotel = 'hotel',
};

export type Charge = Record<ChargeTypes, number>;

export type DistrictCharge = Record<StreetType, Charge>;
