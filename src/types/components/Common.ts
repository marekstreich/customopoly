import { Town } from "../town";

export type TitleProps = {
	town: Town;
	hideSubtitle?: boolean;
};
