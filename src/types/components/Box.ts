import { Town } from "../town";

export type BoxProps = {
	town: Town;
};
