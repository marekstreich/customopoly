export enum BleedColor {
	dark = 'dark',
	light = 'light',
};

export type BleedProps = {
	color?: BleedColor;
	size?: number;
}
