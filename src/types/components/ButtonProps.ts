export type ButtonProps = {
	color: 'primary' | 'secondary';
}
