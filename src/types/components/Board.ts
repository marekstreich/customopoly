import {
	FieldType,
	ChanceFieldNumber,
	CompanyFieldNumber,
	StationFieldNumber,
	StreetFieldNumber,
	TaxFieldNumber,
} from '../field';
import { Town } from '../town';
import { DistrictColor } from '../district';
import { CompanyIcon } from '../company';
import { TaxIcon } from '../tax';

export type ChanceFieldProps = {
	type: FieldType.chance,
	number: ChanceFieldNumber;
};

export type CompanyFieldProps = {
	type: FieldType.company,
	name: string;
	number: CompanyFieldNumber;
	icon: CompanyIcon;
};

export type StationFieldProps = {
	type: FieldType.station
	name: string;
	number: StationFieldNumber;
};

export type StreetFieldProps = {
	type: FieldType.street,
	name: string;
	number: StreetFieldNumber;
	color: DistrictColor;
	price: number;
};

export type StreetFieldColor = Pick<StreetFieldProps, 'color'>

export type TaxFieldProps = {
	type: FieldType.tax,
	name: string,
	number: TaxFieldNumber;
	icon: TaxIcon,
};

export type FieldProps = ChanceFieldProps
	| CompanyFieldProps
	| StationFieldProps
	| StreetFieldProps
	| TaxFieldProps;

export type BoardProps = {
	town: Town;
};

export type BoardSideProps = {
	fields: FieldProps[];
};
