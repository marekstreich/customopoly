export enum InfoIconsTheme {
	dark = 'dark',
	light = 'light',
};

export type InfoIconProps = {
	title: string;
	icon: JSX.Element;
	value: string;
	infoIconTheme: InfoIconsTheme;
};

export type InfoIconThemable = Pick<InfoIconProps, 'infoIconTheme'>;

export type InfoIconsProps = {
	infoIconTheme: InfoIconsTheme;
};
