import { Town } from '../town';
import { DistrictColor } from '../district';

export enum PrintablePagesAlign {
	left = 'left',
	right = 'right',
};

export type PrintablePagesProps = {
	title?: string;
	singlePageWidth?: string;
	singlePageHeight?: string;
	align?: PrintablePagesAlign;
};

export type PrintablePageProps = {
	width?: string;
	height?: string;
};

export type PrintablePagesWithCardsByColorsProps = {
	town: Town,
	colors: DistrictColor[],
};

export type PrintablePagesWithChancesProps = {
	town: Town,
};

export type PrintablePagesWithSpecialCardsProps = {
	town: Town,
};

export type PrintablePageWithBoardProps = {
	town: Town,
};

export type PrintablePagesWithBoxesProps = {
	town: Town,
};
