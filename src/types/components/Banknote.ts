import { BanknoteValue } from "../banknote";

export type BanknoteProps = {
	value: BanknoteValue;
}
