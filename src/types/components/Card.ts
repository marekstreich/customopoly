import { DistrictColor } from '../district';
import { Charge } from '../charge';
import { Company } from '../company';
import { Station } from '../station';
import { StreetFieldNumber } from '../field';
import { ChanceType } from '../chance';

export type CardPropertyProps = {
	name: string;
	number: StreetFieldNumber;
	district: string;
	color: DistrictColor;
	price: number;
	charge: Charge;
};

export type CardBackProps = {
	name: string;
	district?: string;
	price: number;
};

export type CardCompanyProps = Company;

export type CardStationProps = Station;

export type CardPropertyColor = Pick<CardPropertyProps, 'color'>;

export type CardChanceProps = {
	text?: string;
	type: ChanceType
	showBleeds?: boolean;
};

export type CardChanceType = Pick<CardChanceProps, 'type'>;
