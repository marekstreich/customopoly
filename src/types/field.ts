export enum FieldType {
	start = 'start',
	jail = 'jail',
	parking = 'parking',
	goToJail = 'goToJail',
	chance = 'chance',
	company = 'company',
	station = 'station',
	street = 'street',
	tax = 'tax',
};

export type StartFieldNumber = 1;
export type JailFieldNumber = 11;
export type ParkingFieldNumber = 21;
export type GoToJailFieldNumber = 31;

export type ChanceFieldNumber = 3 | 8 | 18 | 23 | 34 | 37;
export type CompanyFieldNumber = 13 | 29;
export type StationFieldNumber = 6 | 16 | 26 | 36;
export type StreetFieldNumber = 2 | 4 | 7 | 9 | 10 | 12 | 14 | 15 | 17 | 19 | 20 | 22 | 24 | 25 | 27 | 28 | 30 | 32 | 33 | 35 | 38 | 40;
export type TaxFieldNumber = 5 | 39;

export type SideFieldNumber = ChanceFieldNumber
	| CompanyFieldNumber
	| StationFieldNumber
	| StreetFieldNumber
	| TaxFieldNumber;

export type CornerFieldNumber = StartFieldNumber
	| JailFieldNumber
	| ParkingFieldNumber
	| GoToJailFieldNumber;

export type FieldNumber = SideFieldNumber | CornerFieldNumber;

export type FieldTypeMap = Record<FieldType, FieldNumber[]>;

export const FIELD_TYPE_MAP: FieldTypeMap = {
	[FieldType.start]: [1],
	[FieldType.jail]: [11],
	[FieldType.parking]: [21],
	[FieldType.goToJail]: [31],
	[FieldType.chance]: [3, 8, 18, 23, 34, 37],
	[FieldType.company]: [13, 29],
	[FieldType.station]: [6, 16, 26, 36],
	[FieldType.street]: [2, 4, 7, 9, 10, 12, 14, 15, 17, 19, 20, 22, 24, 25, 27, 28, 30, 32, 33, 35, 38, 40],
	[FieldType.tax]: [5, 39],
};