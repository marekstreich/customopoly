import React, { ReactNode } from "react";
import {
	FaParking,
	FaPiggyBank,
	FaMoneyBillAlt,
	FaDollarSign,
	FaDonate,
} from 'react-icons/fa';
import { TaxFieldNumber } from "./field";

export enum TaxIcon {
	parking = 'parking',
	piggyBank = 'piggyBank',
	cash = 'cash',
	dollar = 'dollar',
	donate = 'donate',
};

export type Tax = {
	name: string;
	number: TaxFieldNumber;
	icon: TaxIcon;
}

export type TaxIconMap = Record<TaxIcon, ReactNode>;

export const TAX_ICON_MAP: TaxIconMap = {
	[TaxIcon.parking]: <FaParking />,
	[TaxIcon.piggyBank]: <FaPiggyBank />,
	[TaxIcon.cash]: <FaMoneyBillAlt />,
	[TaxIcon.dollar]: <FaDollarSign />,
	[TaxIcon.donate]: <FaDonate />,
};

export type TaxNumberPriceMap = Record<TaxFieldNumber, number>;

export const TAX_NUMBER_PRICE_MAP: TaxNumberPriceMap = {
	5: 400,
	39: 200,
};