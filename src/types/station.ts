import { StationFieldNumber } from './field';

export type Station = {
	name: string;
	number: StationFieldNumber;
}