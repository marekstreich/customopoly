import { StreetType } from './street';

export type DistrictPrice = Record<StreetType, number>;
