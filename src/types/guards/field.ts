import { FieldType } from '../field';
import {
	ChanceFieldProps,
	CompanyFieldProps,
	StationFieldProps,
	StreetFieldProps,
	TaxFieldProps,
	FieldProps,
} from '../components/Board';

const isChance = (field: FieldProps): field is ChanceFieldProps => {
    return field.type === FieldType.chance;
};

const isCompany = (field: FieldProps): field is CompanyFieldProps => {
    return field.type === FieldType.company;
};

const isStation = (field: FieldProps): field is StationFieldProps => {
    return field.type === FieldType.station;
};

const isStreet = (field: FieldProps): field is StreetFieldProps => {
    return field.type === FieldType.street;
};

const isTax = (field: FieldProps): field is TaxFieldProps => {
    return field.type === FieldType.tax;
};

export {
	isChance,
	isCompany,
	isStation,
	isStreet,
	isTax,
};
