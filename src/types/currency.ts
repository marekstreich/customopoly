export enum CurrencyCode {
	PLN = 'PLN',
	USD = 'USD',
	EUR = 'EUR',
	GBP = 'GBP',
	ILS = 'ILS',
	JPY = 'JPY',
};

export type Currency = {
	symbol: string;
	case?: 'lowercase' | 'uppercase';
	position: 'left' | 'right';
	space: boolean;
};

export type SupportedCurrencies = Record<CurrencyCode, Currency>;

export const SUPPORTED_CURRENCIES: SupportedCurrencies = {
	[CurrencyCode.PLN]: {
		symbol: 'zł',
		case: 'lowercase',
		position: 'right',
		space: true,
	},
	[CurrencyCode.USD]: {
		symbol: '$',
		position: 'left',
		space: false,
	},
	[CurrencyCode.EUR]: {
		symbol: '€',
		position: 'left',
		space: false,
	},
	[CurrencyCode.GBP]: {
		symbol: '£',
		position: 'left',
		space: false,
	},
	[CurrencyCode.ILS]: {
		symbol: '₪',
		position: 'left',
		space: true,
	},
	[CurrencyCode.JPY]: {
		symbol: '¥',
		position: 'left',
		space: true,
	},
};
