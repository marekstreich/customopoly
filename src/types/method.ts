
import { ChanceSet, ChanceType } from "./chance";
import { District, DistrictColor } from "./district";
import {
	CompanyFieldNumber,
	StationFieldNumber,
	StreetFieldNumber,
	SideFieldNumber,
	TaxFieldNumber,
} from "./field";
import { Street, StreetType } from "./street";
import { Town } from "./town";
import { PrintablePagesWithCardsByColorsProps } from "./components/PrintablePage";
import { StreetWithDistrict } from "./intersection";

export type GetChanceCardsProps = {
	chances: ChanceSet;
};

export type ChanceCard = {
	key: string;
	text: string;
};

export type GetChanceCardsReturnType = {
	[ChanceType.blue]?: ChanceCard[],
	[ChanceType.red]?: ChanceCard[],
};

export type GetCompanyByFieldNumberProps = {
	number: CompanyFieldNumber;
	town: Town;
};

export type GetFieldPropsArrayProps = {
	start: SideFieldNumber;
	end: SideFieldNumber;
	town: Town;
};

export type GetPriceByColorAndStreetTypeProps = {
	color: DistrictColor,
	type: StreetType,
	town: Town,
};

export type GetStationByFieldNumberProps = {
	number: StationFieldNumber,
	town: Town,
};

export type GetStreetCardsProps = {
	districts: District[],
};

export type GetStreetCardsReturnType = StreetWithDistrict[];

export type GetStreetWithDistrictProps = {
	street: Street,
	district: District,
};

export type GetStreetWithDistrictByColorsProps = PrintablePagesWithCardsByColorsProps & {
	reverseChunkLength?: number;
};

export type GetStreetWithDistrictByFieldNumberProps = {
	number: StreetFieldNumber,
	town: Town,
};

export type GetTaxByFieldNumberProps = {
	number: TaxFieldNumber,
	town: Town,
};