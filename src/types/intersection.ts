import { Street } from './street';
import { District } from './district';

export type StreetWithDistrict = Street
	& Pick<District, 'color'>
	& { district: District['name'] };