import React, { ReactNode } from 'react';
import {
	FaBolt,
	FaCandyCane,
	FaChartPie,
	FaCog,
	FaFeather,
	FaFish,
	FaFlask,
	FaFutbol,
	FaGasPump,
	FaHeartbeat,
	FaIndustry,
	FaRadiation,
	FaShip,
	FaStar,
	FaTractor,
} from 'react-icons/fa';
import { colors } from '../utilities/styles';
import { CompanyFieldNumber } from './field';

export enum CompanyIcon {
	bolt = 'bolt',
	candy = 'candy',
	chart = 'chart',
	cog = 'cog',
	feather = 'feather',
	fish = 'fish',
	flask = 'flask',
	football = 'football',
	gas = 'gas',
	heartBeat = 'heartBeat',
	industry = 'industry',
	radiation = 'radiation',
	ship = 'ship',
	star = 'star',
	tractor = 'tractor',
};

export type Company = {
	name: string;
	number: CompanyFieldNumber;
	icon: CompanyIcon;
}

export type CompanyIconMap = Record<CompanyIcon, ReactNode>;

export const COMPANY_ICON_MAP: CompanyIconMap = {
	[CompanyIcon.bolt]: <FaBolt />,
	[CompanyIcon.candy]: <FaCandyCane />,
	[CompanyIcon.chart]: <FaChartPie />,
	[CompanyIcon.cog]: <FaCog />,
	[CompanyIcon.feather]: <FaFeather />,
	[CompanyIcon.fish]: <FaFish />,
	[CompanyIcon.flask]: <FaFlask />,
	[CompanyIcon.football]: <FaFutbol />,
	[CompanyIcon.gas]: <FaGasPump />,
	[CompanyIcon.heartBeat]: <FaHeartbeat />,
	[CompanyIcon.industry]: <FaIndustry />,
	[CompanyIcon.radiation]: <FaRadiation />,
	[CompanyIcon.ship]: <FaShip />,
	[CompanyIcon.star]: <FaStar />,
	[CompanyIcon.tractor]: <FaTractor />,
};

export type CompanyNumberColorMap = Record<CompanyFieldNumber, string>;

export const COMPANY_NUMBER_COLOR_MAP: CompanyNumberColorMap = {
	13: colors.GAME.darkRed,
	29: colors.GAME.darkBlue,
};