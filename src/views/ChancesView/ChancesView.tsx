import React, { FunctionComponent } from 'react';
import View, { ViewTitle, ViewContent } from '../../components/View/View';
import PrintableLayout from '../../components/Printable/PrintableLayout';
import PrintablePagesWithChances from '../../components/Printable/PrintablePagesWithChances';
import stargard from '../../data/stargard';

const ChancesView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Chances</ViewTitle>
			<ViewContent>
				<PrintableLayout>
					<PrintablePagesWithChances town={stargard} />
				</PrintableLayout>
			</ViewContent>
		</View>
	);
}

export default ChancesView;
