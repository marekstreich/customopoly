import React, { FunctionComponent } from 'react';
import View, { ViewTitle, ViewContent } from '../../components/View/View';
import PrintableLayout from '../../components/Printable/PrintableLayout';
import PrintablePagesWithBoxes from '../../components/Printable/PrintablePagesWithBoxes';
import stargard from '../../data/stargard';

const BoxView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Box</ViewTitle>
			<ViewContent>
				<PrintableLayout>
					<PrintablePagesWithBoxes town={stargard} />
				</PrintableLayout>
			</ViewContent>
		</View>
	);
}

export default BoxView;
