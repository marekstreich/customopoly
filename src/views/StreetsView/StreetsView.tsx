import React, { FunctionComponent } from 'react';
import View, { ViewTitle, ViewContent } from '../../components/View/View';
import PrintableLayout from '../../components/Printable/PrintableLayout';
import PrintablePagesWithCardsByColors from '../../components/Printable/PrintablePagesWithCardsByColors';
import PrintablePagesWithSpecialCards from '../../components/Printable/PrintablePagesWithSpecialCards';
import { DistrictColor } from '../../types/district';
import stargard from '../../data/stargard';

const StreetsView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Streets</ViewTitle>
			<ViewContent>
				<PrintableLayout>
					<PrintablePagesWithCardsByColors town={stargard} colors={[
						DistrictColor.yellow,
						DistrictColor.orange,
						DistrictColor.red,
					]} />
					<PrintablePagesWithCardsByColors town={stargard} colors={[
						DistrictColor.pink,
						DistrictColor.purple,
						DistrictColor.blue,
					]} />
					<PrintablePagesWithCardsByColors town={stargard} colors={[
						DistrictColor.teal,
						DistrictColor.green,
					]} />
					<PrintablePagesWithSpecialCards town={stargard} />
				</PrintableLayout>
			</ViewContent>
		</View>
	);
}

export default StreetsView;
