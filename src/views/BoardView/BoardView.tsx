import React, { FunctionComponent } from 'react';
import View, { ViewTitle, ViewContent } from '../../components/View/View';
import PrintableLayout from '../../components/Printable/PrintableLayout';
import PrintablePageWithBoard from '../../components/Printable/PrintablePageWithBoard';
import stargard from '../../data/stargard';

const BoardView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Board</ViewTitle>
			<ViewContent>
				<PrintableLayout>
					<PrintablePageWithBoard town={stargard} />
				</PrintableLayout>
			</ViewContent>
		</View>
	);
}

export default BoardView;
