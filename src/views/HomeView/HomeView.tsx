import React, { FunctionComponent } from 'react';
import View, { ViewTitle } from '../../components/View/View';

const HomeView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Home</ViewTitle>
		</View>
	);
}

export default HomeView;
