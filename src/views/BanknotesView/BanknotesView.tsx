import React, { FunctionComponent } from 'react';
import View, { ViewTitle, ViewContent } from '../../components/View/View';
import PrintableLayout from '../../components/Printable/PrintableLayout';
import PrintablePagesWithBanknotes from '../../components/Printable/PrintablePagesWithBanknotes';

const BanknotesView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Banknotes</ViewTitle>
			<ViewContent>
				<PrintableLayout>
					<PrintablePagesWithBanknotes />
				</PrintableLayout>
			</ViewContent>
		</View>
	);
}

export default BanknotesView;
