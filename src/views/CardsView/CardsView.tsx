import React, { FunctionComponent } from 'react';
import View, { ViewTitle, ViewContent } from '../../components/View/View';
import PrintableLayout from '../../components/Printable/PrintableLayout';
import PrintablePagesWithInlineChances from '../../components/Printable/PrintablePagesWithInlineChances';
import PrintablePagesWithInlineCards from '../../components/Printable/PrintablePagesWithInlineCards';
import stargard from '../../data/stargard';

const CardsView: FunctionComponent = () => {
	return (
		<View>
			<ViewTitle>Cards</ViewTitle>
			<ViewContent>
				<PrintableLayout>
					<PrintablePagesWithInlineCards town={stargard} />
					<PrintablePagesWithInlineChances town={stargard} />
				</PrintableLayout>
			</ViewContent>
		</View>
	);
}

export default CardsView;
