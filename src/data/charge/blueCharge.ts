import { ChargeTypes, DistrictCharge } from '../../types/charge';

const blueCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 45,
		[ChargeTypes.house1]: 220,
		[ChargeTypes.house2]: 660,
		[ChargeTypes.house3]: 1600,
		[ChargeTypes.house4]: 1950,
		[ChargeTypes.hotel]: 2300,
	},
	main: {
		[ChargeTypes.empty]: 50,
		[ChargeTypes.house1]: 240,
		[ChargeTypes.house2]: 720,
		[ChargeTypes.house3]: 1700,
		[ChargeTypes.house4]: 2050,
		[ChargeTypes.hotel]: 2400,
	},
}

export default blueCharge;