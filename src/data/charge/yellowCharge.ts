import { ChargeTypes, DistrictCharge } from '../../types/charge';

const yellowCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 5,
		[ChargeTypes.house1]: 40,
		[ChargeTypes.house2]: 70,
		[ChargeTypes.house3]: 180,
		[ChargeTypes.house4]: 320,
		[ChargeTypes.hotel]: 500,
	},
	main: {
		[ChargeTypes.empty]: 10,
		[ChargeTypes.house1]: 50,
		[ChargeTypes.house2]: 120,
		[ChargeTypes.house3]: 360,
		[ChargeTypes.house4]: 640,
		[ChargeTypes.hotel]: 900,
	},
}

export default yellowCharge;