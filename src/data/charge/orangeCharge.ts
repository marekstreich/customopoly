import { ChargeTypes, DistrictCharge } from '../../types/charge';

const orangeCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 15,
		[ChargeTypes.house1]: 60,
		[ChargeTypes.house2]: 180,
		[ChargeTypes.house3]: 540,
		[ChargeTypes.house4]: 800,
		[ChargeTypes.hotel]: 1100,
	},
	main: {
		[ChargeTypes.empty]: 20,
		[ChargeTypes.house1]: 80,
		[ChargeTypes.house2]: 200,
		[ChargeTypes.house3]: 600,
		[ChargeTypes.house4]: 900,
		[ChargeTypes.hotel]: 1200,
	},
}

export default orangeCharge;