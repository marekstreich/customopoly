import { TownCharge } from '../../types/town';
import { DistrictColor } from '../../types/district';
import { default as yellowCharge } from './yellowCharge';
import { default as orangeCharge } from './orangeCharge';
import { default as redCharge } from './redCharge';
import { default as pinkCharge } from './pinkCharge';
import { default as purpleCharge } from './purpleCharge';
import { default as blueCharge } from './blueCharge';
import { default as tealCharge } from './tealCharge';
import { default as greenCharge } from './greenCharge';

const charge: TownCharge = {
	[DistrictColor.yellow]: yellowCharge,
	[DistrictColor.orange]: orangeCharge,
	[DistrictColor.red]: redCharge,
	[DistrictColor.pink]: pinkCharge,
	[DistrictColor.purple]: purpleCharge,
	[DistrictColor.blue]: blueCharge,
	[DistrictColor.teal]: tealCharge,
	[DistrictColor.green]: greenCharge,
};

export default charge;