import { ChargeTypes, DistrictCharge } from '../../types/charge';

const pinkCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 30,
		[ChargeTypes.house1]: 140,
		[ChargeTypes.house2]: 400,
		[ChargeTypes.house3]: 1100,
		[ChargeTypes.house4]: 1500,
		[ChargeTypes.hotel]: 1900,
	},
	main: {
		[ChargeTypes.empty]: 35,
		[ChargeTypes.house1]: 160,
		[ChargeTypes.house2]: 440,
		[ChargeTypes.house3]: 1200,
		[ChargeTypes.house4]: 1600,
		[ChargeTypes.hotel]: 2000,
	},
}

export default pinkCharge;