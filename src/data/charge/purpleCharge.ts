import { ChargeTypes, DistrictCharge } from '../../types/charge';

const purpleCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 35,
		[ChargeTypes.house1]: 180,
		[ChargeTypes.house2]: 500,
		[ChargeTypes.house3]: 1400,
		[ChargeTypes.house4]: 1750,
		[ChargeTypes.hotel]: 2100,
	},
	main: {
		[ChargeTypes.empty]: 40,
		[ChargeTypes.house1]: 200,
		[ChargeTypes.house2]: 600,
		[ChargeTypes.house3]: 1500,
		[ChargeTypes.house4]: 1850,
		[ChargeTypes.hotel]: 2200,
	},
}

export default purpleCharge;