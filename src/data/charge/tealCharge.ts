import { ChargeTypes, DistrictCharge } from '../../types/charge';

const tealCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 55,
		[ChargeTypes.house1]: 260,
		[ChargeTypes.house2]: 780,
		[ChargeTypes.house3]: 1900,
		[ChargeTypes.house4]: 2200,
		[ChargeTypes.hotel]: 2550,
	},
	main: {
		[ChargeTypes.empty]: 60,
		[ChargeTypes.house1]: 280,
		[ChargeTypes.house2]: 840,
		[ChargeTypes.house3]: 2000,
		[ChargeTypes.house4]: 2350,
		[ChargeTypes.hotel]: 2700,
	},
}

export default tealCharge;