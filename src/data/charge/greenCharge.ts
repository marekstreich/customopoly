import { ChargeTypes, DistrictCharge } from '../../types/charge';

const greenCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 70,
		[ChargeTypes.house1]: 350,
		[ChargeTypes.house2]: 1000,
		[ChargeTypes.house3]: 2200,
		[ChargeTypes.house4]: 2600,
		[ChargeTypes.hotel]: 3000,
	},
	main: {
		[ChargeTypes.empty]: 100,
		[ChargeTypes.house1]: 400,
		[ChargeTypes.house2]: 1200,
		[ChargeTypes.house3]: 2800,
		[ChargeTypes.house4]: 3400,
		[ChargeTypes.hotel]: 4000,
	},
}

export default greenCharge;