import { ChargeTypes, DistrictCharge } from '../../types/charge';

const redCharge: DistrictCharge = {
	regular: {
		[ChargeTypes.empty]: 20,
		[ChargeTypes.house1]: 100,
		[ChargeTypes.house2]: 300,
		[ChargeTypes.house3]: 900,
		[ChargeTypes.house4]: 1250,
		[ChargeTypes.hotel]: 1500,
	},
	main: {
		[ChargeTypes.empty]: 25,
		[ChargeTypes.house1]: 120,
		[ChargeTypes.house2]: 360,
		[ChargeTypes.house3]: 1000,
		[ChargeTypes.house4]: 1400,
		[ChargeTypes.hotel]: 1800,
	},
}

export default redCharge;