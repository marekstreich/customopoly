
import { Station } from '../../../types/station';

const witkowo: Station = {
	name: 'Pętla Witkowo',
	number: 6,
}

const zcp: Station = {
	name: 'Centrum Przesiadkowe',
	number: 16,
}

const tesco: Station = {
	name: 'Pętla Tesco',
	number: 26,
}

const placWolnosci: Station = {
	name: 'Plac Wolności',
	number: 36,
}

const stations: Station[] = [
	witkowo,
	zcp,
	tesco,
	placWolnosci,
];

export default stations;