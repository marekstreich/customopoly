
import { Tax, TaxIcon } from '../../../types/tax';

const mpgk: Tax = {
	name: 'Urząd Skarbowy',
	number: 5,
	icon: TaxIcon.piggyBank,
}

const spp: Tax = {
	name: 'SPP',
	number: 39,
	icon: TaxIcon.parking,
}

const taxes: Tax[] = [
	mpgk,
	spp,
];

export default taxes;