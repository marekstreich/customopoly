
import { Company, CompanyIcon } from '../../../types/company';

const parkPrzemysłowy: Company = {
	name: 'Park Przemysłowy',
	number: 13,
	icon: CompanyIcon.industry,
}

const polskiCukier: Company = {
	name: 'Polski Cukier',
	number: 29,
	icon: CompanyIcon.candy,
}

const companies: Company[] = [
	parkPrzemysłowy,
	polskiCukier,
];

export default companies;