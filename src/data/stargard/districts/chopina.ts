
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const czeska: Street = {
	name: 'Czeska',
	number: 27,
	type: StreetType.regular,
};

const chopina: Street = {
	name: 'Chopina',
	number: 28,
	type: StreetType.regular,
};

const wieniawskiego: Street = {
	name: 'Wieniawskiego',
	number: 30,
	type: StreetType.main,
};

const osiedleChopina: District = {
	name: 'Chopina',
	color: DistrictColor.blue,
	streets: [czeska, chopina, wieniawskiego],
};

export default osiedleChopina;