
import { District } from '../../../types/district';
import { default as kluczewo } from './kluczewo';
import { default as kossaka } from './kossaka';
import { default as letnie } from './letnie';
import { default as przedmiescia } from './przedmiescia';
import { default as pyrzyckie } from './pyrzyckie';
import { default as chopina } from './chopina';
import { default as centrum } from './centrum';
import { default as starowka } from './starowka';

const districts: District[] = [
	kluczewo,
	kossaka,
	letnie,
	przedmiescia,
	pyrzyckie,
	chopina,
	centrum,
	starowka,
];

export default districts;