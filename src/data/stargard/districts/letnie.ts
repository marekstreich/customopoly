
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const placMajdanek: Street = {
	name: 'Plac Majdanek',
	number: 12,
	type: StreetType.regular,
};

const oswiaty: Street = {
	name: 'Oświaty',
	number: 14,
	type: StreetType.regular,
};

const pogodna: Street = {
	name: 'Pogodna',
	number: 15,
	type: StreetType.main,
};

const letnie: District = {
	name: 'Letnie',
	color: DistrictColor.red,
	streets: [placMajdanek, oswiaty, pogodna],
};

export default letnie;