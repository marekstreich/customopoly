
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const pilsudskiego: Street = {
	name: 'Piłsudskiego',
	number: 32,
	type: StreetType.regular,
};

const czarnieckiego: Street = {
	name: 'Czarnieckiego',
	number: 33,
	type: StreetType.regular,
};

const wyszynskiego: Street = {
	name: 'Wyszyńskiego',
	number: 35,
	type: StreetType.main,
};

const centrum: District = {
	name: 'Centrum',
	color: DistrictColor.teal,
	streets: [pilsudskiego, czarnieckiego, wyszynskiego],
};

export default centrum;