
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const zlotnikow: Street = {
	name: 'Złotników',
	number: 38,
	type: StreetType.regular,
};

const chrobrego: Street = {
	name: 'Chrobrego',
	number: 40,
	type: StreetType.main,
};

const starowka: District = {
	name: 'Starówka',
	color: DistrictColor.green,
	streets: [zlotnikow, chrobrego],
};

export default starowka;