
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const lipnik: Street = {
	name: 'Lipnik',
	number: 17,
	type: StreetType.regular,
};

const grzedzice: Street = {
	name: 'Grzędzice',
	number: 19,
	type: StreetType.regular,
};

const morzyczyn: Street = {
	name: 'Morzyczyn',
	number: 20,
	type: StreetType.main,
};

const przedmiescia: District = {
	name: 'Przedmieścia',
	color: DistrictColor.pink,
	streets: [lipnik, grzedzice, morzyczyn],
};

export default przedmiescia;