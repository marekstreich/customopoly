
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const malczewskiego: Street = {
	name: 'Malczewskiego',
	number: 7,
	type: StreetType.regular,
};

const matejki: Street = {
	name: 'Matejki',
	number: 9,
	type: StreetType.regular,
};

const podlesna: Street = {
	name: 'Podleśna',
	number: 10,
	type: StreetType.main,
};

const kossaka: District = {
	name: 'Kossaka',
	color: DistrictColor.orange,
	streets: [malczewskiego, matejki, podlesna],
};

export default kossaka;