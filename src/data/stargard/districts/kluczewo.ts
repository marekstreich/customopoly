
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const kolejowa: Street = {
	name: 'Kolejowa',
	number: 2,
	type: StreetType.regular,
};

const tanskiego: Street = {
	name: 'Tańskiego',
	number: 4,
	type: StreetType.main,
};

const kluczewo: District = {
	name: 'Kluczewo',
	color: DistrictColor.yellow,
	streets: [kolejowa, tanskiego],
};

export default kluczewo;