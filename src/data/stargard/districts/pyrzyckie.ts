
import { District } from '../../../types/district';
import { Street, StreetType } from '../../../types/street';
import { DistrictColor } from '../../../types/district';

const broniewskiego: Street = {
	name: 'Broniewskiego',
	number: 22,
	type: StreetType.regular,
};

const armiiKrajowej: Street = {
	name: 'Armii Krajowej',
	number: 24,
	type: StreetType.regular,
};

const niepodleglosci: Street = {
	name: 'Niepodległości',
	number: 25,
	type: StreetType.main,
};

const pyrzyckie: District = {
	name: 'Pyrzyckie',
	color: DistrictColor.purple,
	streets: [broniewskiego, armiiKrajowej, niepodleglosci],
};

export default pyrzyckie;