
import { ChanceEarnSet } from '../../../../types/chance';

const earn: ChanceEarnSet = {
	once20: 'Źle wydana reszta u Bułatów. Dostajesz bonus 20 PLN.',
	once30: 'Wygrywasz 30 PLN w lotki w Fanaberii.',
	once50: 'Sprzedajesz obrączkę u złotnika na Zachodzie. Dostajesz 50 PLN.',
	once100: 'Mike wymienia Ci zbitą szybę w telefonie. Zyskujesz 100 PLN.',
	once200: 'Wygrywasz konkurs księgarni Żołnierzyk. Dostajesz 200 PLN nagrody.',
	once300: 'Wygrywasz w konkursie w Galerii Starówka. Zgarniasz nagrodę w wysokości 300 PLN. ',
	once400: 'Sprzedajesz działkę w Witkowie. Zarabiasz okrągłe 400 PLN.',
	getFromEachPlayer: 'Masz urodziny. Dostajesz od każdego gracza prezent po 30 PLN.',
};

export default earn;