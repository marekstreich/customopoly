
import { ChancePaySet } from '../../../../types/chance';

const pay: ChancePaySet = {
	once20: 'Dajesz 20 PLN żulowi spod Darkatu.',
	once30: 'Kieszonkowiec w Zodiaku. Tracisz 30 PLN.',
	once50: 'Zabierasz rodzinę na mecz Błękitnych. Płacisz 50 PLN za bilety.',
	once100: 'Chińczyk dla całej rodziny. Zostawiasz 100 PLN w Złotym Smoku.',
	once200: 'Orżnęli Cię na rynku na Reja. Płacisz 200 PLN za rzodkiewkę.',
	once300: 'Biegasz po Placu Wolności. Płacisz mandat 300 PLN.',
	once400: 'Przyjęcie w La Stradzie. Płacisz 400 PLN za obszczany kibel.',
	repairHouses: 'Remont zakamienionych rur. Płacisz 80 PLN za każdy dom i 230 PLN za każdy hotel.',
};

export default pay;