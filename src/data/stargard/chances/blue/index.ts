
import { ChanceBlueSet } from '../../../../types/chance';
import custom from './custom';
import earn from './earn';
import pay from './pay';
import go from './go';

const chanceBlueSet: ChanceBlueSet = {
	custom,
	earn,
	pay,
	go,
};

export default chanceBlueSet;