
import { ChanceCustomSet } from '../../../../types/chance';

const custom: ChanceCustomSet = {
	custom1: 'Zasypiasz w autobusie. Budzisz się na Tańskiego.',
	custom2: 'Hot dog z prażoną cebulką w Barze Przystań? Jedziesz do Morzyczyna.',
	custom3: 'Nauka gry w tenisa. Jedziesz na Broniewskiego.',
	custom4: 'Zakupy w Galerii Starówka. Idziesz na Chrobrego.',
	custom5: 'Barman orżnął Cię w Rock Housie. Tracisz 100 PLN.',
	custom6: 'Dajesz w palnik na weselu Dominiki i Rafała. Idziesz zygzakiem 3 pola do przodu.',
	custom7: 'Zrywasz tyrolkę na placu zabaw. Płacisz 200 PLN za odbudowę.',
	custom8: 'Znajdujesz kopertę ze ślubu Dominiki i Rafała. Dostajesz 500 PLN.',
};

export default custom;