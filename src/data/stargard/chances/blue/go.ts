
import { ChanceGoBlueSet } from '../../../../types/chance';

const go: ChanceGoBlueSet = {
	goToStart: 'Idziesz na start.',
	backToStart: 'Wracasz na start.',
	goToJail: 'Bombelek przyniósł Covida z przedszkola. Idziesz na kwarantannę.',
	go2Fields: 'Idziesz 2 pola do przodu.',
	back3Fields: 'Cofasz się o 3 pola.',
	goToOrangeDistrict: 'Rarytas rozdaje herbatniki. Idziesz na Podleśną.',
	goToRedDistrict: 'Urodziny bombelka. Jedziesz na kulki na Pogodną.',
	goToBlueDistrict: 'Pierogi u Grażynki. Idziesz zjeść na Chopina.',
	goToTealDistrict: 'Burczy w brzuszku? Idziesz na kebsa na Wyszyńskiego.',
	goToStation2: 'Marzysz o wycieczce PKS\'em. Idziesz do ZCP.',
	goToStation3: 'Zakupy świąteczne. Jedziesz na Pętlę Tesco.',
	goToBlueCompany: 'Zwiedzanie Polskiego Cukru. Płacisz 240 PLN jeśli pole jest zajęte.',
};

export default go;