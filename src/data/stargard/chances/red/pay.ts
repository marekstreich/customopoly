
import { ChancePaySet } from '../../../../types/chance';

const pay: ChancePaySet = {
	once20: 'Kupujesz Big Maca z Colą na Szczecińskiej. Płacisz 20 PLN.',
	once30: 'Kupujesz bułki w piekarni na Szczecińskiej. Płacisz 30 PLN.',
	once50: 'Piwko w Browarze Stargard. Płacisz 50 PLN rachunku.',
	once100: 'Nela robi kupę w Parku Piastowskim. Dostajesz mandat 100 PLN.',
	once200: 'Powrót SPP w Stargardzie. Płacisz 200 PLN mandatu za parkowanie.',
	once300: 'Remont instalacji elektrycznej. Zostawiasz 300 PLN w Elektrze.',
	once400: 'Wjeżdżasz autem w chwilówkę. Płacisz mandat 400 PLN.',
	repairHouses: 'Ina zalała Twoje nieruchomości. Płacisz 80 PLN za każdy dom i 230 PLN za każdy hotel.',
};

export default pay;