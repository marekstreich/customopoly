
import { ChanceGoRedSet } from '../../../../types/chance';

const go: ChanceGoRedSet = {
	goToStart: 'Idziesz na start.',
	backToStart: 'Wracasz na start.',
	goToJail: 'Łapiesz Covida w Panterze. Idziesz na kwarantannę.',
	go3Fields: 'Idziesz 3 pola do przodu.',
	back2Fields: 'Cofasz się o 2 pola.',
	goToYellowDistrict: 'Zapominasz przybić pionę swoim ziomkom. Cofasz się na Kolejową.',
	goToPinkDistrict: 'Gubisz majty w Miedwiu. Wracasz do Morzyczyna.',
	goToPurpleDistrict: 'Łapiesz gumę. Wracasz na Niepodległości do wulkanizacji.',
	goToGreenDistrict: 'Zapominasz drożdży. Wracasz do Kingi na Złotników.',
	goToStation1: 'Badasz pochodzenie smrodu w mieście. Wracasz na Pętlę Witkowo.',
	goToStation4: 'Zapominasz o premierze w SCK. Wracasz na Plac Wolności.',
	goToRedCompany: 'Biznesy w Parku Przemysłowym. Płacisz 240 PLN jeśli pole jest zajęte.',
};

export default go;