
import { ChanceCustomSet } from '../../../../types/chance';

const custom: ChanceCustomSet = {
	custom1: 'Dominika organizuje wieczór z planszówkami. Idziesz na Matejki.',
	custom2: 'Haratasz w gałę na Orliku. Idziesz na Plac Majdanek.',
	custom3: 'Idziesz sadzić drzewa na Wieniawskiego.',
	custom4: 'Premiera filmu Papryka Vegi w SCK. Idziesz na Piłsudskiego.',
	custom5: 'Kamyk robi kupę w Parku Piastowskim. Dostajesz mandat 100 PLN.',
	custom6: 'Gubisz osobę towarzyszącą na weselu Dominiki i Rafała. Cofasz się o 4 pola.',
	custom7: 'Nocne bieganie po rondzie kończy się mandatem w wysokości 200 PLN.',
	custom8: 'Wygrywasz w loterii szczepionkowej. Dostajesz 500 PLN.',
};

export default custom;