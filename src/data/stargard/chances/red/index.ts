
import { ChanceRedSet } from '../../../../types/chance';
import custom from './custom';
import earn from './earn';
import pay from './pay';
import go from './go';

const chanceRedSet: ChanceRedSet = {
	custom,
	earn,
	pay,
	go,
};

export default chanceRedSet;