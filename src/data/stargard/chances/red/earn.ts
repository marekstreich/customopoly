
import { ChanceEarnSet } from '../../../../types/chance';

const earn: ChanceEarnSet = {
	once20: 'Dostajesz 20 PLN do wydania na Lody Bez Nazwy.',
	once30: 'Wygrywasz 30 PLN na automatach w Antidotum.',
	once50: 'Wygrywasz karaoke w Eclipsie. Dostajesz 50 PLN.',
	once100: 'Bijesz rekord Escape Roomu w Bastei. Dostajesz 100 PLN nagrody.',
	once200: 'Znajdujesz 200 PLN na Rynku Staromiejskim.',
	once300: 'Łamiesz nogę wstając z łóżka. Dostajesz odszkodowanie 300 PLN.',
	once400: 'Trafiasz z połowy boiska na Spójni. Dostajesz nagrodę 400 PLN.',
	getFromEachPlayer: 'Masz imieniny. Dostajesz od każdego gracza prezent po 20 PLN.',
};

export default earn;