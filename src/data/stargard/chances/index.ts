
import { ChanceType, ChanceSet } from '../../../types/chance';
import chanceRedSet from './red';
import chanceBlueSet from './blue';

const chances: ChanceSet = {
	[ChanceType.red]: chanceRedSet,
	[ChanceType.blue]: chanceBlueSet,
};

export default chances;