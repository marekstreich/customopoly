import { Town } from '../../types/town';
import charge from '../charge';
import price from '../price';
import districts from './districts';
import companies from './companies';
import stations from './stations';
import taxes from './taxes';
import chances from './chances';

const stargard: Town = {
	name: 'Stargard',
	meta: {
		title: 'Stargardzki Biznes',
		subtitle: 'Zostań szychą biznesu w swoim mieście',
		creator: 'Kinga Ziembicka-Streich',
	},
	charge,
	price,
	districts,
	companies,
	stations,
	taxes,
	chances,
};

export default stargard;