import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const yellowPrice: DistrictPrice = {
	[StreetType.regular]: 120,
	[StreetType.main]: 120,
}

export default yellowPrice;