import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const purplePrice: DistrictPrice = {
	[StreetType.regular]: 440,
	[StreetType.main]: 480,
}

export default purplePrice;