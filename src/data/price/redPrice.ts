import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const redPrice: DistrictPrice = {
	[StreetType.regular]: 280,
	[StreetType.main]: 320,
}

export default redPrice;