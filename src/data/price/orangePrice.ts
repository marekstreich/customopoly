import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const orangePrice: DistrictPrice = {
	[StreetType.regular]: 200,
	[StreetType.main]: 240,
}

export default orangePrice;