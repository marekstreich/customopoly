import { TownPrice } from '../../types/town';
import { DistrictColor } from '../../types/district';
import { default as yellowPrice } from './yellowPrice';
import { default as orangePrice } from './orangePrice';
import { default as redPrice } from './redPrice';
import { default as pinkPrice } from './pinkPrice';
import { default as purplePrice } from './purplePrice';
import { default as bluePrice } from './bluePrice';
import { default as tealPrice } from './tealPrice';
import { default as greenPrice } from './greenPrice';

const price: TownPrice = {
	[DistrictColor.yellow]: yellowPrice,
	[DistrictColor.orange]: orangePrice,
	[DistrictColor.red]: redPrice,
	[DistrictColor.pink]: pinkPrice,
	[DistrictColor.purple]: purplePrice,
	[DistrictColor.blue]: bluePrice,
	[DistrictColor.teal]: tealPrice,
	[DistrictColor.green]: greenPrice,
};

export default price;