import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const greenPrice: DistrictPrice = {
	[StreetType.regular]: 700,
	[StreetType.main]: 800,
}

export default greenPrice;