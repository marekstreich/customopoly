import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const bluePrice: DistrictPrice = {
	[StreetType.regular]: 520,
	[StreetType.main]: 560,
}

export default bluePrice;