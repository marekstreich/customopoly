import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const pinkPrice: DistrictPrice = {
	[StreetType.regular]: 360,
	[StreetType.main]: 400,
}

export default pinkPrice;