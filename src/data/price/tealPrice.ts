import { DistrictPrice } from '../../types/price';
import { StreetType } from '../../types/street';

const tealPrice: DistrictPrice = {
	[StreetType.regular]: 600,
	[StreetType.main]: 640,
}

export default tealPrice;