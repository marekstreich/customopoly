import React, { FunctionComponent } from 'react';
import { useIsBelowDesktop } from '../../../utilities/hooks';
import AppBelowDesktopNav from './AppBelowDesktopNav/AppBelowDesktopNav';
import AppDesktopNav from './AppDesktopNav/AppDesktopNav';

const AppNav: FunctionComponent = () => {
	const isBelowDesktop = useIsBelowDesktop();
	return isBelowDesktop ? <AppBelowDesktopNav /> : <AppDesktopNav />;
}

export default AppNav;
