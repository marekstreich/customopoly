import React, { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../../Logo/Logo';
import { Nav, LogoWrapper, List, Item } from './AppDesktopNav.styled';

const AppDesktopNav: FunctionComponent = () => {
	return (
		<Nav>
			<LogoWrapper>
				<Logo />
			</LogoWrapper>
			<List>
				<Item>
					<Link to="/">Home</Link>
				</Item>
				<Item>
					<Link to="/board">Board</Link>
				</Item>
				<Item>
					<Link to="/banknotes">Banknotes</Link>
				</Item>
				<Item>
					<Link to="/cards">Cards</Link>
				</Item>
				<Item>
					<Link to="/chances">Chances</Link>
				</Item>
				<Item>
					<Link to="/box">Box</Link>
				</Item>
				<Item>
					<Link to="/streets">Streets</Link>
				</Item>
			</List>
		</Nav>
	);
}

export default AppDesktopNav;
