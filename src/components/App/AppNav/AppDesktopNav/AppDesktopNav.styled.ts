import styled from '@emotion/styled';
import { NAV_WIDTH, NAV_SPACING } from '../../App.constants';
import { colors } from '../../../../utilities/styles';

const Nav = styled.div`
	position: fixed;
	top: 0;
	width: ${NAV_WIDTH}rem;
	height:  100%;
	left: 0;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
	overflow: auto;
	background: linear-gradient(${colors.WHITE.a025}, ${colors.WHITE.a075});
	box-shadow: 0 0 2rem rgba(0, 0, 0, 0.2);
	z-index: 1;
`;

const LogoWrapper = styled.div`
	margin: 0;
	padding: ${NAV_SPACING}rem;
`;

const List = styled.ul`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
	margin: 0;
	padding: 0;
	list-style-type: none;
`;

const Item = styled.li`
	display: flex;
	flex-direction: row;

	a {
		flex: 1;
		padding: ${NAV_SPACING * 0.5}rem ${NAV_SPACING}rem;
		text-decoration: none;
		color: ${colors.JET.a600};
		transition: color 0.2s, background 0.2s;

		&:hover {
			color: ${colors.WHITE.a025};
			background: ${colors.MINT.a600};
			transition: color 0.2s, background 0.2s;
		}
	}
`;

export {
	Nav,
	LogoWrapper,
	List,
	Item,
}