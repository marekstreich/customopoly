import styled from '@emotion/styled';
import { NAV_HEIGHT, NAV_SPACING } from '../../App.constants';
import { colors } from '../../../../utilities/styles';

const Nav = styled.ul`
	position: fixed;
	height: ${NAV_HEIGHT}rem;
	right: 0;
	bottom: 0;
	left: 0;
	margin: 0;
	padding: 0 ${NAV_SPACING}rem;
	list-style-type: none;
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	align-items: stretch;
	color: ${colors.JET.a400};
	background: linear-gradient(90deg, ${colors.WHITE.a025}, ${colors.WHITE.a075});
	box-shadow: 0 0 2rem rgba(0, 0, 0, 0.2);
	z-index: 1;
`;

const Item = styled.li`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: stretch;

	a {
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
		text-decoration: none;
		color: ${colors.JET.a600};
		transition: color 0.2s;

		&:hover {
			color: ${colors.JET.a400};
			transition: color 0.2s;
		}
	}
`;

export {
	Nav,
	Item,
}