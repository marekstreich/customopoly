import React, { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import { Nav, Item } from './AppBelowDesktopNav.styled';

const AppBelowDesktopNav: FunctionComponent = () => {
	return (
		<Nav>
			<Item>
				<Link to="/">Home</Link>
			</Item>
			<Item>
				<Link to="/board">Board</Link>
			</Item>
			<Item>
				<Link to="/banknotes">Banknotes</Link>
			</Item>
			<Item>
				<Link to="/cards">Cards</Link>
			</Item>
			<Item>
				<Link to="/chances">Chances</Link>
			</Item>
			<Item>
				<Link to="/box">Box</Link>
			</Item>
			<Item>
				<Link to="/streets">Streets</Link>
			</Item>
		</Nav>
	);
}

export default AppBelowDesktopNav;
