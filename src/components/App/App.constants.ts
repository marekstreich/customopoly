const NAV_WIDTH = 16;
const NAV_HEIGHT = 4;
const NAV_SPACING = 1;

export {
	NAV_WIDTH,
	NAV_HEIGHT,
	NAV_SPACING,
};
