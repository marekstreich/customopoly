import React, { FunctionComponent } from 'react';
import { Switch, Route } from 'react-router-dom';
import { useIsBelowDesktop } from '../../../utilities/hooks';
import BanknotesView from '../../../views/BanknotesView/BanknotesView';
import BoardView from '../../../views/BoardView/BoardView';
import BoxView from '../../../views/BoxView/BoxView';
import CardsView from '../../../views/CardsView/CardsView';
import ChancesView from '../../../views/ChancesView/ChancesView';
import HomeView from '../../../views/HomeView/HomeView';
import StreetsView from '../../../views/StreetsView/StreetsView';
import { BelowDesktopViewsWrapper, DesktopViewsWrapper } from './AppViews.styled';

const ViewsWrapper: FunctionComponent = ({ children }) => {
	const isBelowDesktop = useIsBelowDesktop();
	return isBelowDesktop
		? (<BelowDesktopViewsWrapper>{children}</BelowDesktopViewsWrapper>)
		: (<DesktopViewsWrapper>{children}</DesktopViewsWrapper>);
}

const AppViews: FunctionComponent = () => {
	return (
		<ViewsWrapper>
			<Switch>
				<Route path="/banknotes">
					<BanknotesView />
				</Route>
				<Route path="/board">
					<BoardView />
				</Route>
				<Route path="/box">
					<BoxView />
				</Route>
				<Route path="/cards">
					<CardsView />
				</Route>
				<Route path="/chances">
					<ChancesView />
				</Route>
				<Route path="/streets">
					<StreetsView />
				</Route>
				<Route path="/">
					<HomeView />
				</Route>
			</Switch>
		</ViewsWrapper>
	);
}

export default AppViews;
