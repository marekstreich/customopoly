import styled from '@emotion/styled';
import { NAV_WIDTH, NAV_HEIGHT } from '../App.constants';

const DesktopViewsWrapper = styled.div`
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: ${NAV_WIDTH}rem;
	overflow-x: hidden;
	overflow-y: auto;
`;

const BelowDesktopViewsWrapper = styled.div`
	position: fixed;
	top: 0;
	right: 0;
	bottom: ${NAV_HEIGHT}rem;
	left: 0;
	overflow-x: hidden;
	overflow-y: auto;
`;

export {
	DesktopViewsWrapper,
	BelowDesktopViewsWrapper,
}