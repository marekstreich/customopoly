import React, { FunctionComponent } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { CurrencyCode } from '../../types/currency';
import CurrencyContext from '../Currency/CurrencyContext';
import AppNav from './AppNav/AppNav';
import AppViews from './AppViews/AppViews';
import { Wrapper } from './App.styled';

const App: FunctionComponent = () => {
	return (
		<CurrencyContext.Provider value={CurrencyCode.PLN}>
			<Router>
				<Wrapper>
					<AppNav />
					<AppViews />
				</Wrapper>
			</Router>
		</CurrencyContext.Provider>
	);
}

export default App;
