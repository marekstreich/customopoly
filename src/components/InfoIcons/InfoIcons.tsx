import React, { FunctionComponent } from 'react';
import { FaChild, FaUserFriends, FaClock } from 'react-icons/fa';
import { InfoIconsProps, InfoIconProps } from '../../types/components/InfoIcons';
import { Icons, Wrapper, Title, Icon, Value } from './InfoIcons.styled';

const InfoIcon: FunctionComponent<InfoIconProps> = ({ title, icon, value, infoIconTheme }) => {
	return (
		<Wrapper infoIconTheme={infoIconTheme}>
			<Title infoIconTheme={infoIconTheme}>{title}</Title>
			<Icon infoIconTheme={infoIconTheme}>{icon}</Icon>
			<Value infoIconTheme={infoIconTheme}>{value}</Value>
		</Wrapper>
	)
};

const InfoIcons: FunctionComponent<InfoIconsProps> = ({ infoIconTheme }) => {
	return (
		<Icons>
			<InfoIcon
				title="wiek graczy"
				icon={<FaChild />}
				value="+8"
				infoIconTheme={infoIconTheme}
			/>
			<InfoIcon
				title="liczba graczy"
				icon={<FaUserFriends />}
				value="2-5"
				infoIconTheme={infoIconTheme}
			/>
			<InfoIcon
				title="czas rozgrywki"
				icon={<FaClock />}
				value="45-60 min"
				infoIconTheme={infoIconTheme}
			/>
		</Icons>
	);
}

export default InfoIcons;
