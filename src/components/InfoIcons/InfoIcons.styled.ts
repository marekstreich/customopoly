import styled from '@emotion/styled';
import { InfoIconsTheme, InfoIconThemable } from '../../types/components/InfoIcons';
import { colors, fontFamilies } from '../../utilities/styles';

const SPACING = 0.4;
const TITLE_SIZE = 0.15;
const ICON_SIZE = 0.7;
const VALUE_SIZE = 0.3;

const Icons = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
`;

const Wrapper = styled.div<InfoIconThemable>`
	overflow: hidden;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
	min-width: ${TITLE_SIZE * 12}cm;
	margin-right: ${SPACING}cm;
	border: 1px ${({ infoIconTheme }) => infoIconTheme === InfoIconsTheme.light ? colors.GAME.white : colors.GAME.gray} solid;
	border-radius: ${SPACING * 0.5}cm;

	&:last-child {
		margin-right: 0;
	}
`;

const TitleAndValue = styled.h5<InfoIconThemable>`
	margin: 0;
	text-align: center;
	text-transform: uppercase;
	font-weight: 700;
	font-family: ${fontFamilies.dosis};
`;

const Title = styled(TitleAndValue)`
	padding: ${TITLE_SIZE * 0.5 + SPACING * 0.25}cm ${TITLE_SIZE}cm ${TITLE_SIZE * 0.5}cm;
	font-size: ${TITLE_SIZE}cm;
	line-height: ${TITLE_SIZE}cm;
	color: ${({ infoIconTheme }) => infoIconTheme === InfoIconsTheme.light ? colors.GAME.white : colors.GAME.gray};
`;

const Icon = styled.div<InfoIconThemable>`
	display: flex;
	justify-content: center;
	align-items: center;
	margin-bottom: ${TITLE_SIZE * 0.5 + SPACING * 0.25}cm;

	svg {
		width: ${ICON_SIZE}cm;
		height: ${ICON_SIZE}cm;
		color: ${({ infoIconTheme }) => infoIconTheme === InfoIconsTheme.light ? colors.GAME.white : colors.GAME.gray};
	}
`;

const Value = styled(TitleAndValue)`
	padding: ${VALUE_SIZE * 0.5}cm ${VALUE_SIZE}cm;
	font-size: ${VALUE_SIZE}cm;
	line-height: ${VALUE_SIZE}cm;
	color: ${({ infoIconTheme }) => infoIconTheme === InfoIconsTheme.light ? colors.GAME.sky : colors.GAME.white};
	background: ${({ infoIconTheme }) => infoIconTheme === InfoIconsTheme.light ? colors.GAME.white : colors.GAME.gray};
`;

export {
	Icons,
	Wrapper,
	Title,
	Icon,
	Value,
};
