import React, { FunctionComponent } from 'react';
import { PrintablePagesWithBoxesProps } from '../../types/components/PrintablePage';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';
import { BOX_SIZE_X_WITH_BLEEDS, BOX_SIZE_Y_WITH_BLEEDS, BOX_SIZE_Z_WITH_BLEEDS } from '../Box/Box.constants';
import BoxTop from '../Box/BoxTop/BoxTop';
import BoxBottom from '../Box/BoxBottom/BoxBottom';
import BoxSide1 from '../Box/BoxSide1/BoxSide1';
import BoxSide2 from '../Box/BoxSide2/BoxSide2';
import BoxSides from '../Box/BoxSides/BoxSides';

const PrintablePagesWithBoxes: FunctionComponent<PrintablePagesWithBoxesProps> = ({ town }) => {
	return (
		<>
			<PrintablePages
				title="Top of the box"
				singlePageWidth={`${BOX_SIZE_X_WITH_BLEEDS}cm`}
				singlePageHeight={`${BOX_SIZE_Y_WITH_BLEEDS}cm`}
			>
				<PrintablePage width={`${BOX_SIZE_X_WITH_BLEEDS}cm`} height={`${BOX_SIZE_Y_WITH_BLEEDS}cm`}>
					<BoxTop town={town} />
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title="Bottom of the box"
				singlePageWidth={`${BOX_SIZE_X_WITH_BLEEDS}cm`}
				singlePageHeight={`${BOX_SIZE_Y_WITH_BLEEDS}cm`}
			>
				<PrintablePage width={`${BOX_SIZE_X_WITH_BLEEDS}cm`} height={`${BOX_SIZE_Y_WITH_BLEEDS}cm`}>
					<BoxBottom town={town} />
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title="First and third side"
				singlePageWidth={`${BOX_SIZE_X_WITH_BLEEDS}cm`}
				singlePageHeight={`${BOX_SIZE_Z_WITH_BLEEDS}cm`}
			>
				<PrintablePage width={`${BOX_SIZE_X_WITH_BLEEDS}cm`} height={`${BOX_SIZE_Z_WITH_BLEEDS}cm`}>
					<BoxSide1 town={town} />
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title="Second and fourth side"
				singlePageWidth={`${BOX_SIZE_Y_WITH_BLEEDS}cm`}
				singlePageHeight={`${BOX_SIZE_Z_WITH_BLEEDS}cm`}
			>
				<PrintablePage width={`${BOX_SIZE_Y_WITH_BLEEDS}cm`} height={`${BOX_SIZE_Z_WITH_BLEEDS}cm`}>
					<BoxSide2 town={town} />
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title="Sides"
				singlePageWidth={`${BOX_SIZE_Y_WITH_BLEEDS}cm`}
				singlePageHeight={`${BOX_SIZE_Z_WITH_BLEEDS * 4}cm`}
			>
				<PrintablePage width={`${BOX_SIZE_Y_WITH_BLEEDS}cm`} height={`${BOX_SIZE_Z_WITH_BLEEDS * 4}cm`}>
					<BoxSides town={town} />
				</PrintablePage>
			</PrintablePages>
		</>
	);
}

export default PrintablePagesWithBoxes;
