import styled from '@emotion/styled';

const MARGIN = 1;

const Wrapper = styled.div`
	box-sizing: border-box;
	flex: 1;
	height: 100%;
	padding: ${MARGIN}cm;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: space-between;
	align-content: center;
	align-items: flex-start;
`;

export {
	Wrapper,
};
