import React, { FunctionComponent } from 'react';
import { PrintablePageProps } from '../../types/components/PrintablePage';
import { Page } from './PrintablePage.styled';

const PrintablePage: FunctionComponent<PrintablePageProps> = ({ children, width, height }) => {
	return (
		<Page width={width} height={height}>{children}</Page>
	);
}

export default PrintablePage;
