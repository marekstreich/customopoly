import React, { FunctionComponent } from 'react';
import { PrintablePageWithBoardProps } from '../../types/components/PrintablePage';
import Board from '../Board/Board';
import { BOARD_SIZE_WITH_BLEEDS } from '../Board/Board.constants';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const PrintablePageWithBoard: FunctionComponent<PrintablePageWithBoardProps> = ({ town }) => {
	return (
		<PrintablePages title="Board" singlePageWidth={`${BOARD_SIZE_WITH_BLEEDS}cm`} singlePageHeight="auto">
			<PrintablePage width={`${BOARD_SIZE_WITH_BLEEDS}cm`} height={`${BOARD_SIZE_WITH_BLEEDS}cm`}>
				<Board town={town} />
			</PrintablePage>
		</PrintablePages>
	);
}

export default PrintablePageWithBoard;
