import styled from '@emotion/styled';
import { PRINTABLE_PAGE_MARGIN } from './Printable.constants';

const Pages = styled.div`
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: flex-start;
	align-items: flex-start;
	margin: 0 ${PRINTABLE_PAGE_MARGIN * -0.5}rem;

	> * {
		margin: 0 ${PRINTABLE_PAGE_MARGIN * 0.5}rem ${PRINTABLE_PAGE_MARGIN}rem;
	}
`;

export {
	Pages,
};
