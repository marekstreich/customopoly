import React, { FunctionComponent } from 'react';
import { getStreetWithDistrictByColors, reverseArrayChunks } from '../../utilities/methods';
import { PrintablePagesWithCardsByColorsProps } from '../../types/components/PrintablePage';
import CardProperty from '../Card/CardProperty/CardProperty';
import CardBack from '../Card/CardBack/CardBack';
import { A4_WIDTH, A4_HEIGHT } from './Printable.constants';
import PrintableLayoutA4 from './PrintableLayoutA4';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const PrintablePagesWithCardsByColors: FunctionComponent<PrintablePagesWithCardsByColorsProps> = ({ town, colors }) => {
	const properties = getStreetWithDistrictByColors({ town, colors });
	return (
		<>
			<PrintablePages
				title={`Streets front: ${colors.map((color) => color).join(', ')}`}
				singlePageWidth={`${A4_WIDTH}cm`}
				singlePageHeight={`${A4_HEIGHT}cm`}
			>
				<PrintablePage width={`${A4_WIDTH}cm`} height={`${A4_HEIGHT}cm`}>
					<PrintableLayoutA4>
						{properties.map((street) => <CardProperty
							key={street.name}
							name={street.name}
							number={street.number}
							district={street.district}
							color={street.color}
							price={town.price[street.color][street.type]}
							charge={town.charge[street.color][street.type]}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title={`Streets back: ${colors.map((color) => color).join(', ')}`}
				singlePageWidth={`${A4_WIDTH}cm`}
				singlePageHeight={`${A4_HEIGHT}cm`}
			>
				<PrintablePage width={`${A4_WIDTH}cm`} height={`${A4_HEIGHT}cm`}>
					<PrintableLayoutA4>
						{reverseArrayChunks(properties, 3).map((street) => <CardBack
							key={street.name}
							name={street.name}
							district={street.district}
							price={town.price[street.color][street.type]}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>
		</>
	);
}

export default PrintablePagesWithCardsByColors;
