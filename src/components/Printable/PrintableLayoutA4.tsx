import React, { FunctionComponent } from 'react';
import { Wrapper } from './PrintableLayoutA4.styled';

const PrintableLayoutA4: FunctionComponent = ({ children }) => {
	return (
		<Wrapper>{children}</Wrapper>
	);
}

export default PrintableLayoutA4;
