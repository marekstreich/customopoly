import React, { FunctionComponent, Fragment } from 'react';
import { PrintablePagesWithChancesProps } from '../../types/components/PrintablePage';
import { ChanceType } from '../../types/chance';
import { getChanceCards } from '../../utilities/methods/getCards';
import CardChance from '../Card/CardChance/CardChance';
import { CARD_HEIGHT } from '../Card/Card.constants';
import { BLEED_SIZE } from '../Bleeds/Bleeds.constants';
import PrintablePages from './PrintablePages';

const PrintablePagesWithInlineChances: FunctionComponent<PrintablePagesWithChancesProps> = ({
	town: {
		chances,
	},
}) => {
	const pageWidth = CARD_HEIGHT + 2 * BLEED_SIZE;
	const cards = getChanceCards({ chances });
	return (
		<>
			<PrintablePages title="Red chances" singlePageWidth={`${pageWidth}cm`}>
				{cards?.red?.map(({ key, text }) => <Fragment key={key}>
					<CardChance text={text} type={ChanceType.red} />
					<CardChance type={ChanceType.red} />
				</Fragment>)}
			</PrintablePages>
			<PrintablePages title="Blue chances" singlePageWidth={`${pageWidth}cm`}>
				{cards?.blue?.map(({ key, text }) => <Fragment key={key}>
					<CardChance text={text} type={ChanceType.blue} />
					<CardChance type={ChanceType.blue} />
				</Fragment>)}
			</PrintablePages>
		</>
	);
}

export default PrintablePagesWithInlineChances;
