import React, { FunctionComponent } from 'react';
import { PrintablePagesWithChancesProps } from '../../types/components/PrintablePage';
import { ChanceType } from '../../types/chance';
import { getArrayWithPageNumbers, paginate } from '../../utilities/methods';
import { getChanceCards } from '../../utilities/methods/getCards';
import CardChance from '../Card/CardChance/CardChance';
import { A4_WIDTH, A4_HEIGHT } from './Printable.constants';
import PrintableLayoutA4 from './PrintableLayoutA4';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const PAGE_SIZE = 9;

const PrintablePagesWithChances: FunctionComponent<PrintablePagesWithChancesProps> = ({
	town: {
		chances,
	},
}) => {
	const cards = getChanceCards({ chances });
	const blueCardsPagesCount = Math.ceil((cards.blue?.length || 0) / PAGE_SIZE);
	const redCardsPagesCount = Math.ceil((cards.red?.length || 0) / PAGE_SIZE);
	const backCards = getArrayWithPageNumbers(PAGE_SIZE);
	const blueCardsPages = getArrayWithPageNumbers(blueCardsPagesCount);
	const redCardsPages = getArrayWithPageNumbers(redCardsPagesCount);

	return (
		<>
			<PrintablePages
				title="Blue chances front"
				singlePageWidth={`${A4_HEIGHT}cm`}
				singlePageHeight={`${A4_WIDTH}cm`}
			>
				{blueCardsPages.map((pageNumber) => <PrintablePage
					key={pageNumber}
					width={`${A4_HEIGHT}cm`}
					height={`${A4_WIDTH}cm`}
				>
					<PrintableLayoutA4>
						{paginate(cards?.blue || [], PAGE_SIZE, pageNumber + 1)?.map(({ key, text }) => <CardChance
							key={key}
							text={text}
							type={ChanceType.blue}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>)}
			</PrintablePages>
			<PrintablePages
				title="Blue chances back"
				singlePageWidth={`${A4_HEIGHT}cm`}
				singlePageHeight={`${A4_WIDTH}cm`}
			>
				<PrintablePage width={`${A4_HEIGHT}cm`} height={`${A4_WIDTH}cm`} >
					<PrintableLayoutA4>
						{backCards.map((key) => <CardChance key={key} type={ChanceType.blue} showBleeds={false} />)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title="Red chances front"
				singlePageWidth={`${A4_HEIGHT}cm`}
				singlePageHeight={`${A4_WIDTH}cm`}
			>
				{redCardsPages.map((pageNumber) => <PrintablePage
					key={pageNumber}
					width={`${A4_HEIGHT}cm`}
					height={`${A4_WIDTH}cm`}
				>
					<PrintableLayoutA4>
						{paginate(cards?.red || [], PAGE_SIZE, pageNumber + 1)?.map(({ key, text }) => <CardChance
							key={key}
							text={text}
							type={ChanceType.red}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>)}
			</PrintablePages>
			<PrintablePages
				title="Red chances back"
				singlePageWidth={`${A4_HEIGHT}cm`}
				singlePageHeight={`${A4_WIDTH}cm`}
			>
				<PrintablePage width={`${A4_HEIGHT}cm`} height={`${A4_WIDTH}cm`} >
					<PrintableLayoutA4>
						{backCards.map((key) => <CardChance key={key} type={ChanceType.red} showBleeds={false} />)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>
		</>
	);
}

export default PrintablePagesWithChances;
