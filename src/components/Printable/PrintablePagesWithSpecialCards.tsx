import React, { FunctionComponent } from 'react';
import { reverseArrayChunks } from '../../utilities/methods';
import { PrintablePagesWithSpecialCardsProps } from '../../types/components/PrintablePage';
import CardCompany from '../Card/CardCompany/CardCompany';
import CardStation from '../Card/CardStation/CardStation';
import CardBack from '../Card/CardBack/CardBack';
import { A4_WIDTH, A4_HEIGHT } from './Printable.constants';
import PrintableLayoutA4 from './PrintableLayoutA4';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const PrintablePagesWithSpecialCards: FunctionComponent<PrintablePagesWithSpecialCardsProps> = ({ town }) => {
	return (
		<>
			<PrintablePages
				title="Specal cards front"
				singlePageWidth={`${A4_WIDTH}cm`}
				singlePageHeight={`${A4_HEIGHT}cm`}
			>
				<PrintablePage width={`${A4_WIDTH}cm`} height={`${A4_HEIGHT}cm`}>
					<PrintableLayoutA4>
						{town.stations.map((station) => <CardStation
							key={station.name}
							name={station.name}
							number={station.number}
						/>)}
						{town.companies.map((company) => <CardCompany
							key={company.name}
							name={company.name}
							number={company.number}
							icon={company.icon}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>
			<PrintablePages
				title="Special cards back"
				singlePageWidth={`${A4_WIDTH}cm`}
				singlePageHeight={`${A4_HEIGHT}cm`}
			>
				<PrintablePage width={`${A4_WIDTH}cm`} height={`${A4_HEIGHT}cm`}>
					<PrintableLayoutA4>
						{reverseArrayChunks(town.stations, 3).slice(0, 3).map((station) => <CardBack
							key={station.name}
							name={station.name}
							price={400}
						/>)}
						{reverseArrayChunks(town.companies, 3).map((company) => <CardBack
							key={company.name}
							name={company.name}
							price={300}
						/>)}
						{reverseArrayChunks(town.stations, 3).slice(3, 4).map((station) => <CardBack
							key={station.name}
							name={station.name}
							price={400}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>
		</>
	);
}

export default PrintablePagesWithSpecialCards;
