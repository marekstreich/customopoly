import styled from '@emotion/styled';
import { PrintablePageProps } from '../../types/components/PrintablePage';

const Page = styled.div<PrintablePageProps>`
	display: block;
	width: ${({ width }) => width};
	height: ${({ height }) => height};
	page-break-before: always;
	page-break-after: always;
	break-inside: avoid;
`;

export {
	Page,
};
