import React, { FunctionComponent } from 'react';
import { Pages } from './PrintableLayout.styled';

const PrintableLayout: FunctionComponent = ({ children }) => {
	return (
		<Pages>{children}</Pages>
	);
}

export default PrintableLayout;
