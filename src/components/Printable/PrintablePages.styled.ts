import styled from '@emotion/styled';
import { PrintablePagesAlign, PrintablePagesProps } from '../../types/components/PrintablePage';
import { colors } from '../../utilities/styles';
import Button from '../Button/Button.styled';

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: stretch;
	padding: 1rem;
	border-radius: 0.5rem;
	box-shadow: 0 0.5rem 1.5rem rgba(0, 0, 0, 0.1);
	background: linear-gradient(${colors.WHITE.a025}, ${colors.WHITE.a050});
`;

const Header = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	margin-bottom: 1rem;
`;

const Trigger = styled(Button)`
	margin-right: 1rem;
`;

const Title = styled.h3`
	margin: 0;
	padding: 0;
	font-size: 1rem;
	color: ${colors.WENGE.a300};
`;

const PagesWrapper = styled.div`
	box-shadow: 0 0 0.5rem rgba(0, 0, 0, 0.1);
`;

const Pages = styled.div<PrintablePagesProps>`
	box-sizing: border-box;
	display: flex;
	flex-direction: row;
	justify-content: ${({ align }) => align === PrintablePagesAlign.left ? 'flex-start' : 'flex-end'};
	align-items: flex-start;
	align-content: flex-start;
	flex-wrap: wrap;
	width: ${({ singlePageWidth }) => singlePageWidth};
	height: auto;
	margin: 0;
	padding: 0;
	background: #fff;
`;

export { Wrapper, Header, Trigger, Title, PagesWrapper, Pages };
