import React, { FunctionComponent } from 'react';
import { BanknoteValue } from '../../types/banknote';
import Banknote from '../Banknote/Banknote';
import { A4_WIDTH, A4_HEIGHT } from './Printable.constants';
import PrintableLayoutA4 from './PrintableLayoutA4';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const BANKNOTE_VALUES: BanknoteValue[] = [5, 10, 20, 50, 100, 500, 1000];
const BANKNOTES_PER_PAGE = 12;

const PrintablePagesWithBanknotes: FunctionComponent = () => {
	return (
		<>
			{BANKNOTE_VALUES.map((value) => <PrintablePages
				key={value}
				title={`Banknotes: ${value} PLN`}
				singlePageWidth={`${A4_WIDTH}cm`}
				singlePageHeight={`${A4_HEIGHT}cm`}
			>
				<PrintablePage
					width={`${A4_WIDTH}cm`}
					height={`${A4_HEIGHT}cm`}
				>
					<PrintableLayoutA4>
						{Array.from(Array(BANKNOTES_PER_PAGE).keys()).map((key) => <Banknote
							key={key}
							value={value}
						/>)}
					</PrintableLayoutA4>
				</PrintablePage>
			</PrintablePages>)}
		</>
	);
}

export default PrintablePagesWithBanknotes;
