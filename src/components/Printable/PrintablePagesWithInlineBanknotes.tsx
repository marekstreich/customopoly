import React, { FunctionComponent } from 'react';
import { BanknoteValue } from '../../types/banknote';
import { BLEED_SIZE } from '../Bleeds/Bleeds.constants';
import { BANKNOTE_WIDTH, BANKNOTE_HEIGHT } from '../Banknote/Banknote.constants';
import Banknote from '../Banknote/Banknote';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const BANKNOTE_VALUES: BanknoteValue[] = [5, 10, 20, 50, 100, 500, 1000];

const PrintablePagesWithInlineBanknotes: FunctionComponent = () => {
	const pageWidth = BANKNOTE_WIDTH + 2 * BLEED_SIZE;
	const pageHeight = BANKNOTE_HEIGHT + 2 * BLEED_SIZE;
	return (
		<PrintablePages
			title="Banknotes"
			singlePageWidth={`${pageWidth}cm`}
			singlePageHeight={`${pageHeight}cm`}
		>
			{BANKNOTE_VALUES.map((value) => <PrintablePage
				key={value}
				width={`${pageWidth}cm`}
				height={`${pageHeight}cm`}
			>
				<Banknote value={value} />
			</PrintablePage>)}
		</PrintablePages>
	);
}

export default PrintablePagesWithInlineBanknotes;
