import React, { FunctionComponent, useRef } from 'react';
import { useReactToPrint } from 'react-to-print';
import { FaPrint } from 'react-icons/fa';
import { PrintablePagesAlign, PrintablePagesProps } from '../../types/components/PrintablePage';
import { BLEED_SIZE } from '../Bleeds/Bleeds.constants';
import { Wrapper, Header, Trigger, Title, PagesWrapper, Pages } from './PrintablePages.styled';

const PrintablePages: FunctionComponent<PrintablePagesProps> = ({
	children,
	title,
	singlePageWidth = '21cm',
	singlePageHeight = 'auto',
	align = PrintablePagesAlign.left,
}) => {
	const ref = useRef(null);
	const onTriggerClick = useReactToPrint({
		content: () => ref.current,
		documentTitle: title,
		pageStyle: `
			@media print {
				@page {
					size: ${singlePageWidth} ${singlePageHeight};
					margin: 0cm;
					bleed: ${BLEED_SIZE}cm;
				}
			}
		`,
	});

	return (
		<Wrapper>
			<Header>
				<Trigger color="primary" onClick={onTriggerClick}><FaPrint />Print</Trigger>
				{title && <Title>{title}</Title>}
			</Header>
			<PagesWrapper>
				<Pages
					singlePageWidth={singlePageWidth}
					align={align}
					ref={ref}
				>{children}</Pages>
			</PagesWrapper>
		</Wrapper>
	);
}

export default PrintablePages;