import React, { FunctionComponent, Fragment } from 'react';
import { PrintablePagesWithChancesProps } from '../../types/components/PrintablePage';
import { getStreetCards } from '../../utilities/methods/getCards';
import { CARD_WIDTH, CARD_HEIGHT } from '../Card/Card.constants';
import { BLEED_SIZE } from '../Bleeds/Bleeds.constants';
import CardCompany from '../Card/CardCompany/CardCompany';
import CardFront from '../Card/CardProperty/CardProperty';
import CardBack from '../Card/CardBack/CardBack';
import CardStation from '../Card/CardStation/CardStation';
import PrintablePages from './PrintablePages';
import PrintablePage from './PrintablePage';

const PrintablePagesWithInlineCards: FunctionComponent<PrintablePagesWithChancesProps> = ({
	town: {
		charge,
		companies,
		districts,
		price,
		stations,
	},
}) => {
	const pageWidth = CARD_WIDTH + 2 * BLEED_SIZE;
	const pageHeight = CARD_HEIGHT + 2 * BLEED_SIZE;
	const streets = getStreetCards({ districts: districts });
	return (
		<>
			<PrintablePages
				title="Properties"
				singlePageWidth={`${pageWidth}cm`}
				singlePageHeight={`${pageHeight}cm`}
			>
				{streets.map((street) => <Fragment key={street.number}>
					<PrintablePage width={`${pageWidth}cm`} height={`${pageHeight}cm`}>
						<CardFront
							name={street.name}
							number={street.number}
							district={street.district}
							color={street.color}
							price={price[street.color][street.type]}
							charge={charge[street.color][street.type]}
						/>
					</PrintablePage>
					<PrintablePage width={`${pageWidth}cm`} height={`${pageHeight}cm`}>
						<CardBack
							name={street.name}
							district={street.district}
							price={price[street.color][street.type]}
						/>
					</PrintablePage>
				</Fragment>)}
			</PrintablePages>
			<PrintablePages
				title="Special"
				singlePageWidth={`${pageWidth}cm`}
				singlePageHeight={`${pageHeight}cm`}
			>
				{companies.map((company) => <Fragment key={company.number}>
					<PrintablePage width={`${pageWidth}cm`} height={`${pageHeight}cm`}>
						<CardCompany
							name={company.name}
							number={company.number}
							icon={company.icon}
						/>
					</PrintablePage>
					<PrintablePage width={`${pageWidth}cm`} height={`${pageHeight}cm`}>
						<CardBack
							name={company.name}
							price={300}
						/>
					</PrintablePage>
				</Fragment>)}
				{stations.map((station) => <Fragment key={station.number}>
					<PrintablePage width={`${pageWidth}cm`} height={`${pageHeight}cm`}>
						<CardStation
							name={station.name}
							number={station.number}
						/>
					</PrintablePage>
					<PrintablePage width={`${pageWidth}cm`} height={`${pageHeight}cm`}>
						<CardBack
							name={station.name}
							price={400}
						/>
					</PrintablePage>
				</Fragment>)}
			</PrintablePages>
		</>
	);
}

export default PrintablePagesWithInlineCards;
