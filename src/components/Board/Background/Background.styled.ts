import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';
import { ChanceType, CHANE_TYPE_COLOR_MAP } from '../../../types/chance';
import { shadowPrintFix } from '../../../utilities/styles/mixins';
import { FIELD_WIDTH, FIELDS_ON_SIDE } from '../Board.constants';
import { CARD_WIDTH, CARD_HEIGHT } from '../../Card/Card.constants';
import town from '../../../assets/town.jpg';

const DISTRICT_NAMES_BAR_SIZE = 1;
const TEXT_SIZE = 2;
const CHANCE_FIELD_ICON_SIZE = 2;
const PADDING = 1;
const SHADOW_SIZE = 1;
const SHADOW_COLOR = 'rgba(0, 0, 0, 0.3)';

const Wrapper = styled.div`
	position: relative;
	width: ${FIELD_WIDTH * FIELDS_ON_SIDE}cm;
	height: ${FIELD_WIDTH * FIELDS_ON_SIDE}cm;
	display: flex;
	justify-content: center;
	align-items: center;
	background: repeating-conic-gradient(
		from 150deg,
		${colors.GAME.yellow},
		${colors.GAME.orange},
		${colors.GAME.red},
		${colors.GAME.pink},
		${colors.GAME.purple},
		${colors.GAME.blue},
		${colors.GAME.teal},
		${colors.GAME.green},
		${colors.GAME.yellow}
	);
	box-shadow:
		0 0 0 1px ${colors.GAME.black},
		0 0 0 1px ${colors.GAME.black} inset;
`;

const Content = styled.div`
	position: relative;
	width: ${FIELD_WIDTH * FIELDS_ON_SIDE - 2 * DISTRICT_NAMES_BAR_SIZE}cm;
	height: ${FIELD_WIDTH * FIELDS_ON_SIDE - 2 * DISTRICT_NAMES_BAR_SIZE}cm;
	display: flex;
	justify-content: center;
	align-items: center;
	background: ${colors.GAME.sky} url(${town}) bottom center no-repeat;
	background-size: contain;
	box-shadow:
		0 0 0 1px ${colors.GAME.black},
		0 0 0 1px ${colors.GAME.black} inset,
		0 0 ${SHADOW_SIZE}cm ${SHADOW_COLOR} inset;
	${shadowPrintFix}

	&:before {
		content: '';
		position: absolute;
		top: 1px;
		right: 1px;
		bottom: 1px;
		left: 1px;
		background: linear-gradient(
			0deg,
			rgba(75, 118, 181, 0) 25%,
			${colors.GAME.blue},
			${colors.GAME.purple},
			${colors.GAME.pink}
		);
		${shadowPrintFix}
	}
`;

const ChanceFields = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
	transform: rotate(45deg);
`;

const ChanceField = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: ${CARD_WIDTH + 2 * PADDING}cm;
	height: ${CARD_HEIGHT + 2 * PADDING}cm;
	border-radius: 0.5cm;
	background: white;
	box-shadow: 0 0 ${SHADOW_SIZE}cm ${SHADOW_COLOR};
	${shadowPrintFix}
`;

const RedChanceField = styled(ChanceField)`
	margin-right: ${TEXT_SIZE * 5}cm;

	svg {
		color: ${CHANE_TYPE_COLOR_MAP[ChanceType.red]};
	}
`;

const BlueChanceField = styled(ChanceField)`
	svg {
		color: ${CHANE_TYPE_COLOR_MAP[ChanceType.blue]};
	}
`;

const ChanceFieldIcon = styled.div`
	width: ${CHANCE_FIELD_ICON_SIZE}cm;
	height: ${CHANCE_FIELD_ICON_SIZE}cm;

	svg {
		width: ${CHANCE_FIELD_ICON_SIZE}cm;
		height: ${CHANCE_FIELD_ICON_SIZE}cm;
	}
`;

const Text = styled.div`
	position: absolute;
	font-size: ${TEXT_SIZE}cm;
	line-height: ${TEXT_SIZE}cm;
	color: ${colors.GAME.white};
	font-weight: 700;
	text-transform: uppercase;
	text-shadow: 0 0 ${SHADOW_SIZE}cm ${SHADOW_COLOR};
	text-align: center;
	transform: rotate(315deg);
	max-width: ${CARD_HEIGHT * 2}cm;
`;

export {
	Wrapper,
	Content,
	ChanceFields,
	RedChanceField,
	BlueChanceField,
	ChanceFieldIcon,
	Text,
};
