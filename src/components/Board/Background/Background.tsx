import React, { FunctionComponent } from 'react';
import { ChanceType, CHANCE_TYPE_ICON_MAP } from '../../../types/chance';
import { BoardProps } from '../../../types/components/Board';
import BigTitle from '../../Common/BigTitle/BigTitle';
import DistrictNames from '../DistrictNames/DistrictNames';
import {
	Wrapper,
	Content,
	ChanceFields,
	RedChanceField,
	BlueChanceField,
	ChanceFieldIcon,
	Text,
} from './Background.styled';

const Background: FunctionComponent<BoardProps> = ({ town }) => {
	return (
		<Wrapper>
			<DistrictNames town={town} />
			<Content>
				<ChanceFields>
					<RedChanceField>
						<ChanceFieldIcon>
							{CHANCE_TYPE_ICON_MAP[ChanceType.red]}
						</ChanceFieldIcon>
					</RedChanceField>
					<BlueChanceField>
						<ChanceFieldIcon>
							{CHANCE_TYPE_ICON_MAP[ChanceType.blue]}
						</ChanceFieldIcon>
					</BlueChanceField>
				</ChanceFields>
				<Text>
					<BigTitle town={town} hideSubtitle />
				</Text>
			</Content>
		</Wrapper>
	);
}

export default Background;
