import React, { FunctionComponent, useContext } from 'react';
import { FaArrowCircleLeft } from 'react-icons/fa';
import { CurrencyCode, SUPPORTED_CURRENCIES } from '../../../../types/currency';
import Currency from '../../../Currency/Currency'
import CurrencyContext from '../../../Currency/CurrencyContext';
import { Wrapper, Content, Title, Text, Icon, NumberWrapper, Number } from '../Corner.styled';
import { Signs, Sign, StartBackgroundIcon, StartBg } from './Start.styled';

const CURRENCY_SIGNS_COUNT = 15;

const Start: FunctionComponent = () => {
	const currencyCode: CurrencyCode = useContext(CurrencyContext);
	const currency = SUPPORTED_CURRENCIES[currencyCode];

	return (
		<Wrapper>
			<Signs>
				{Array.from(Array(CURRENCY_SIGNS_COUNT).keys()).map((key) => <Sign key={key}>
					{currency.symbol}
				</Sign>)}
			</Signs>
			<Content>
				<Title>Start</Title>
				<Icon>
					<FaArrowCircleLeft />
				</Icon>
				<Text>Za przejście otrzymujesz <Currency amount={400} /></Text>
			</Content>
			<StartBackgroundIcon>
				<FaArrowCircleLeft />
			</StartBackgroundIcon>
			<StartBg />
			<NumberWrapper style={{ bottom: 0, right: 0 }}>
				<Number style={{ transform: 'rotate(315deg)' }}>1</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default Start;
