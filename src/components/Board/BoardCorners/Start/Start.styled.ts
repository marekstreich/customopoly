import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { HEADER_TEXT_SHADOW } from '../../../Card/Card.constants';
import { Bg, BackgroundIcon } from '../Corner.styled';

const SIGN_SIZE = 0.3;

const Signs = styled.div`
	position: absolute;
	top: ${SIGN_SIZE * 0.5}cm;
	right: ${SIGN_SIZE * 0.5}cm;
	height: ${SIGN_SIZE}cm;
	left: ${SIGN_SIZE * 0.5}cm;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	z-index: 2;
`;

const Sign = styled.div`
	overflow: hidden;
	width: ${SIGN_SIZE}cm;
	height: ${SIGN_SIZE}cm;
	line-height: ${SIGN_SIZE}cm;
	border-radius: ${SIGN_SIZE}cm;
	font-size: ${SIGN_SIZE * 0.5}cm;
	font-weight: 700;
	text-align: center;
	color: ${colors.GAME.green};
	background: ${colors.GAME.white};
	filter: drop-shadow(${HEADER_TEXT_SHADOW});
`;

const StartBackgroundIcon = styled(BackgroundIcon)`
	top: -5%;
	left: -5%;
`;

const StartBg = styled(Bg)`
	background: linear-gradient(225deg, ${colors.GAME.green}, ${colors.GAME.yellow});
`;

export {
	Signs,
	Sign,
	StartBackgroundIcon,
	StartBg,
};
