export { default as FreeParking } from './FreeParking/FreeParking';
export { default as GoToJail } from './GoToJail/GoToJail';
export { default as Start } from './Start/Start';
export { default as Jail } from './Jail/Jail';