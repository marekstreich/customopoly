import React, { FunctionComponent } from 'react';
import { FaBirthdayCake } from 'react-icons/fa';
import { Wrapper, Title, Icon, NumberWrapper, Number } from '../Corner.styled';
import { FreeParkingContent, FreeParkingBackgroundIcon, FreeParkingBg } from './FreeParking.styled';

const FreeParking: FunctionComponent = () => {
	return (
		<Wrapper>
			<FreeParkingContent>
				<Title>Dni</Title>
				<Icon>
					<FaBirthdayCake />
				</Icon>
				<Title>Stargardu</Title>
			</FreeParkingContent>

			<FreeParkingBackgroundIcon>
				<FaBirthdayCake />
			</FreeParkingBackgroundIcon>
			<FreeParkingBg />
			<NumberWrapper style={{ top: 0, left: 0 }}>
				<Number style={{ transform: 'rotate(135deg)' }}>21</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default FreeParking;
