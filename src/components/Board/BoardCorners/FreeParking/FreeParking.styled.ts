import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { Content, BackgroundIcon, Bg } from '../Corner.styled';

const FreeParkingContent = styled(Content)`
	transform: rotate(135deg);
`;

const FreeParkingBackgroundIcon = styled(BackgroundIcon)`
	transform: rotate(135deg);
`;

const FreeParkingBg = styled(Bg)`
	background: linear-gradient(45deg, ${colors.GAME.pink}, ${colors.GAME.purple});
`;

export {
	FreeParkingContent,
	FreeParkingBackgroundIcon,
	FreeParkingBg,
};
