import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { Content, Bg, BackgroundIcon } from '../Corner.styled';

const JailContent = styled(Content)`
	transform: rotate(45deg);
`;

const JailBackgroundIcon = styled(BackgroundIcon)`
	top: -10%;
	transform: rotate(45deg);
`;

const JailBg = styled(Bg)`
	background: linear-gradient(315deg, ${colors.GAME.orange}, ${colors.GAME.red});
`;

export {
	JailContent,
	JailBackgroundIcon,
	JailBg,
};
