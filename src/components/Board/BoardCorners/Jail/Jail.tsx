import React, { FunctionComponent } from 'react';
import { FaSkull } from 'react-icons/fa';
import { Wrapper, Title, Text, Icon, NumberWrapper, Number } from '../Corner.styled';
import { JailContent, JailBackgroundIcon, JailBg } from './Jail.styled';

const BottomLeftCorner: FunctionComponent = () => {
	return (
		<Wrapper>
			<JailContent>
				<Title>Kwarantanna</Title>
				<Icon>
					<FaSkull />
				</Icon>
				<Text>Siedzisz w domu</Text>
				<Text>dwie kolejki</Text>
			</JailContent>
			<JailBackgroundIcon>
				<FaSkull />
			</JailBackgroundIcon>
			<JailBg />
			<NumberWrapper style={{ bottom: 0, left: 0 }}>
				<Number style={{ transform: 'rotate(45deg)' }}>11</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default BottomLeftCorner;
