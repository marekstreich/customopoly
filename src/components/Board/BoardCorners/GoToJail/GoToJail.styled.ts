import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { Content, BackgroundIcon, Bg } from '../Corner.styled';

const GoToJailContent = styled(Content)`
	transform: rotate(225deg);
`;

const GoToJailBackgroundIcon = styled(BackgroundIcon)`
	left: -10%;
	transform: rotate(225deg);
`;

const GoToJailBg = styled(Bg)`
	background: linear-gradient(135deg, ${colors.GAME.blue}, ${colors.GAME.teal});
`;

export {
	GoToJailContent,
	GoToJailBackgroundIcon,
	GoToJailBg,
};
