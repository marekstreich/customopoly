import React, { FunctionComponent } from 'react';
import { FaGlassCheers } from 'react-icons/fa';
import { Wrapper, Title, Text, Icon, NumberWrapper, Number } from '../Corner.styled';
import { GoToJailContent, GoToJailBackgroundIcon, GoToJailBg } from './GoToJail.styled';

const TopRightCorner: FunctionComponent = () => {
	return (
		<Wrapper>
			<GoToJailContent>
				<Title>Mango</Title>
				<Icon>
					<FaGlassCheers />
				</Icon>
				<Text>Łapiesz wirus od Milfa</Text>
				<Text>Trafiasz na kwarantannę</Text>
			</GoToJailContent>
			<GoToJailBackgroundIcon>
				<FaGlassCheers />
			</GoToJailBackgroundIcon>
			<GoToJailBg />
			<NumberWrapper style={{ top: 0, right: 0 }}>
				<Number style={{ transform: 'rotate(225deg)' }}>31</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default TopRightCorner;
