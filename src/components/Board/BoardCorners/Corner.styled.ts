import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';
import { HEADER_TEXT_SHADOW } from '../../Card/Card.constants';
import { Number as BlackNumber, Name } from '../BoardFields/Field.styled';
import { FIELD_HEIGHT, NUMBER_HEIGHT } from '../Board.constants';

const TITLE_SIZE = 0.5;
const ICON_SIZE = 2;

const Wrapper = styled.div`
	position: relative;
	height: ${FIELD_HEIGHT}cm;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

const Content = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
	z-index: 2;
`;

const Title = styled.div`
	font-size: ${TITLE_SIZE}cm;
	line-height: ${TITLE_SIZE}cm;
	font-weight: 700;
	color: ${colors.GAME.white};
	text-transform: uppercase;
	text-shadow: ${HEADER_TEXT_SHADOW};
`;

const Text = styled(Name)`
	color: ${colors.GAME.white};
	text-shadow: ${HEADER_TEXT_SHADOW};
`;

const Icon = styled.div`
	width: ${ICON_SIZE}cm;
	height: ${ICON_SIZE}cm;
	margin: ${TITLE_SIZE}cm 0;
	filter: drop-shadow(${HEADER_TEXT_SHADOW});

	svg {
		width: ${ICON_SIZE}cm;
		height: ${ICON_SIZE}cm;
		color: ${colors.GAME.white};
	}
`;

const BackgroundIcon = styled.div`
	position: absolute;
	top: 0%;
	width: 110%;
	height: 110%;
	left: 0%;
	z-index: 1;

	svg {
		width: 100%;
		height: 100%;
		color: ${colors.GAME.black};
		opacity: 0.05;
	}
`;

const NumberWrapper = styled.div`
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	width: ${NUMBER_HEIGHT}cm;
	height: ${NUMBER_HEIGHT}cm;
	padding: ${NUMBER_HEIGHT * 0.15}cm;
`;

const Number = styled(BlackNumber)`
	color: ${colors.GAME.white};
	text-shadow: ${HEADER_TEXT_SHADOW};
`;

const Bg = styled.div`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: ${colors.GAME.gray};
	box-shadow: 0 0 0 1px ${colors.GAME.black} inset;
`;

export {
	TITLE_SIZE,
	ICON_SIZE,
	Wrapper,
	Content,
	Title,
	Text,
	Icon,
	BackgroundIcon,
	NumberWrapper,
	Number,
	Bg,
};
