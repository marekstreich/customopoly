import React, { FunctionComponent } from 'react';
import { FaBusAlt } from 'react-icons/fa';
import { StationFieldProps } from '../../../../types/components/Board';
import Currency from '../../../Currency/Currency';
import {
	Wrapper,
	Content,
	NumberWrapper,
	IconWrapper,
	Number,
	Info,
	RotatedInfo,
	Name,
	Price,
} from './../Field.styled';

const StationField: FunctionComponent<StationFieldProps> = ({ number, name }) => {
	return (
		<Wrapper>
			<Content>
				<Info>
					<Name>{name}</Name>
					<Price>
						<Currency amount={400} />
					</Price>
				</Info>
				<IconWrapper>
					<FaBusAlt />
				</IconWrapper>
				<RotatedInfo>
					<Name>{name}</Name>
					<Price>
						<Currency amount={400} />
					</Price>
				</RotatedInfo>
			</Content>
			<NumberWrapper>
				<Number>{number}</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default StationField;
