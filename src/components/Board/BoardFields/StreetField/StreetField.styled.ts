import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { StreetFieldColor } from '../../../../types/components/Board';
import {
	FIELD_WIDTH,
	FIELD_HEIGHT,
	HOUSES_HEIGHT,
	NUMBER_HEIGHT,
} from '../../Board.constants';

const HOUSES_GRADIENT_INTENSITY = 0.15;

const Wrapper = styled.div`
	position: relative;
	width: ${FIELD_WIDTH}cm;
	height: ${FIELD_HEIGHT}cm;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
	background: ${colors.GAME.white};
	box-shadow: 0 0 0 1px ${colors.GAME.black} inset;
`;

const Houses = styled.div<StreetFieldColor>`
	position: relative;
	height: ${HOUSES_HEIGHT}cm;
	background: ${({ color }) => colors.GAME[color]};
	box-shadow: 0 0 0 1px ${colors.GAME.black}, 0 0 0 1px ${colors.GAME.black} inset;

	&:before {
		content: '';
		position: absolute;
		top: 1px;
		right: 1px;
		bottom: 1px;
		left: 1px;
		background: linear-gradient(
			25deg,
			rgba(255, 255, 255, ${HOUSES_GRADIENT_INTENSITY}),
			rgba(0, 0, 0, ${HOUSES_GRADIENT_INTENSITY})
		);
	}
`;

const Content = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
`;

const NumberWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: ${NUMBER_HEIGHT}cm;
`;

export {
	Wrapper,
	Houses,
	Content,
	NumberWrapper,
};
