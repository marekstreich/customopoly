import React, { FunctionComponent } from 'react';
import { StreetFieldProps } from '../../../../types/components/Board';
import Currency from '../../../Currency/Currency';
import {
	Wrapper,
	Houses,
	Content,
	NumberWrapper,
} from './StreetField.styled';
import {
	Number,
	Info,
	RotatedInfo,
	Name,
	Price,
} from './../Field.styled';

const StreetField: FunctionComponent<StreetFieldProps> = ({ number, name, color, price }) => {
	return (
		<Wrapper>
			<Houses color={color} />
			<Content>
				<Info>
					<Name>{name}</Name>
					<Price>
						<Currency amount={price} />
					</Price>
				</Info>
				<RotatedInfo>
					<Name>{name}</Name>
					<Price>
						<Currency amount={price} />
					</Price>
				</RotatedInfo>
			</Content>
			<NumberWrapper>
				<Number>{number}</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default StreetField;
