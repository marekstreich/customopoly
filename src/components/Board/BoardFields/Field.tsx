import React, { FunctionComponent } from 'react';
import { FieldProps } from '../../../types/components/Board';
import {
	isChance,
	isCompany,
	isStation,
	isStreet,
	isTax,
} from '../../../types/guards/field';
import ChanceField from './ChanceField/ChanceField';
import CompanyField from './CompanyField/CompanyField';
import StationField from './StationField/StationField';
import StreetField from './StreetField/StreetField';
import TaxField from './TaxField/TaxField';

const Field: FunctionComponent<FieldProps> = (field) => {
	if (isChance(field)) return (<ChanceField {...field} />);
	if (isCompany(field)) return (<CompanyField {...field} />);
	if (isStation(field)) return (<StationField {...field} />);
	if (isStreet(field)) return (<StreetField {...field} />);
	if (isTax(field)) return (<TaxField {...field} />);
	return null;
}

export default Field;
