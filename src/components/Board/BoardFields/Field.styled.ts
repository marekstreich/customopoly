import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';

import {
	FIELD_WIDTH,
	FIELD_HEIGHT,
	NUMBER_HEIGHT,
	FIELD_ICON_SIZE,
} from '../Board.constants';

const Wrapper = styled.div`
	position: relative;
	width: ${FIELD_WIDTH}cm;
	height: ${FIELD_HEIGHT}cm;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: stretch;
	background: ${colors.GAME.white};
	box-shadow: 0 0 0 1px ${colors.GAME.black} inset;
`;

const Content = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
`;

const CenteredContent = styled(Content)`
	justify-content: center;
`;

const NumberWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: ${NUMBER_HEIGHT}cm;
`;

const IconWrapper = styled.div`
	width: ${FIELD_ICON_SIZE}cm;
	height: ${FIELD_ICON_SIZE}cm;
	color: ${colors.GAME.black};

	svg {
		width: ${FIELD_ICON_SIZE}cm;
		height: ${FIELD_ICON_SIZE}cm;
	}
`;

const Number = styled.div`
	font-size: 0.3cm;
	font-weight: 700;
	color: ${colors.GAME.gray};
`;

const Info = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
	text-align: center;
	margin: 0.1cm;
`;

const RotatedInfo = styled(Info)`
	transform: rotate(180deg);
`;

const Name = styled.div`
	text-transform: uppercase;
	font-size: 0.3cm;
	color: ${colors.GAME.black};
`;

const Price = styled(Name)`
	font-weight: 700;
`;

export {
	Wrapper,
	Content,
	CenteredContent,
	NumberWrapper,
	IconWrapper,
	Number,
	Info,
	RotatedInfo,
	Name,
	Price,
};
