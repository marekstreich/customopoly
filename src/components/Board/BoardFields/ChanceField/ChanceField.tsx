import React, { FunctionComponent } from 'react';
import { ChanceFieldProps } from '../../../../types/components/Board';
import { CHANCE_TYPE_NUMBER_MAP, CHANE_TYPE_COLOR_MAP, CHANCE_TYPE_ICON_MAP } from '../../../../types/chance'
import {
	Wrapper,
	CenteredContent,
	NumberWrapper,
	Number,
	IconWrapper,
} from './../Field.styled';

const ChanceField: FunctionComponent<ChanceFieldProps> = ({ number }) => {
	const type = CHANCE_TYPE_NUMBER_MAP[number];
	const icon = CHANCE_TYPE_ICON_MAP[type];
	const color = CHANE_TYPE_COLOR_MAP[type];

	return (
		<Wrapper>
			<CenteredContent>
				<IconWrapper style={{ color }}>
					{icon}
				</IconWrapper>
			</CenteredContent>
			<NumberWrapper>
				<Number>{number}</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default ChanceField;
