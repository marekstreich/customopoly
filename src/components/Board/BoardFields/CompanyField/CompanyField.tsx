import React, { FunctionComponent } from 'react';
import { CompanyFieldProps } from '../../../../types/components/Board';
import { COMPANY_ICON_MAP, COMPANY_NUMBER_COLOR_MAP } from '../../../../types/company';
import Currency from '../../../Currency/Currency';
import {
	Wrapper,
	Content,
	NumberWrapper,
	IconWrapper,
	Number,
	Info,
	RotatedInfo,
	Name,
	Price,
} from './../Field.styled';

const CompanyField: FunctionComponent<CompanyFieldProps> = ({ number, name, icon }) => {
	const iconComponent = COMPANY_ICON_MAP[icon];
	const color = COMPANY_NUMBER_COLOR_MAP[number];

	return (
		<Wrapper>
			<Content>
				<Info>
					<Name>{name}</Name>
					<Price>
						<Currency amount={300} />
					</Price>
				</Info>
				<IconWrapper style={{ color }}>
					{iconComponent}
				</IconWrapper>
				<RotatedInfo>
					<Name>{name}</Name>
					<Price>
						<Currency amount={300} />
					</Price>
				</RotatedInfo>
			</Content>
			<NumberWrapper>
				<Number>{number}</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default CompanyField;
