import React, { FunctionComponent } from 'react';
import { TaxFieldProps } from '../../../../types/components/Board';
import { TAX_ICON_MAP, TAX_NUMBER_PRICE_MAP } from '../../../../types/tax';
import Currency from '../../../Currency/Currency';
import {
	Wrapper,
	Content,
	NumberWrapper,
	Number,
	IconWrapper,
	Info,
	RotatedInfo,
	Name,
	Price,
} from './../Field.styled';

const TaxField: FunctionComponent<TaxFieldProps> = ({ number, name, icon }) => {
	const price = TAX_NUMBER_PRICE_MAP[number];
	const iconComponent = TAX_ICON_MAP[icon];

	return (
		<Wrapper>
			<Content>
				<Info>
					<Name>{name}</Name>
					<Price>
						<Currency amount={price} />
					</Price>
				</Info>
				<IconWrapper>
					{iconComponent}
				</IconWrapper>
				<RotatedInfo>
					<Name>{name}</Name>
					<Price>
						<Currency amount={price} />
					</Price>
				</RotatedInfo>
			</Content>
			<NumberWrapper>
				<Number>{number}</Number>
			</NumberWrapper>
		</Wrapper>
	);
}

export default TaxField;
