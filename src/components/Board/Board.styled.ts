import styled from '@emotion/styled';
import { fontFamilies, colors } from '../../utilities/styles';
import {
	BOARD_BLEED,
	FIELD_WIDTH,
	FIELD_HEIGHT,
	FIELDS_ON_SIDE,
	BOARD_SIZE,
} from './Board.constants';

const BoardBleed = styled.div`
	position: relative;
	overflow: hidden;
	padding: ${BOARD_BLEED}cm;
	background: ${colors.GAME.black};
`;

const BoardWrapper = styled.div`
	position: relative;
	overflow: hidden;
	width: ${BOARD_SIZE}cm;
	height: ${BOARD_SIZE}cm;
	font-family: ${fontFamilies.dosis};
	border-radius: 1.5cm;
`;

const SideWrapper = styled.div`
	position: absolute;
	width: ${FIELD_WIDTH * FIELDS_ON_SIDE}cm;
	height: ${FIELD_HEIGHT}cm;
`;

const TopSideWrapper = styled(SideWrapper)`
	transform: translate(${FIELD_HEIGHT}cm, 0) rotate(180deg);
`;

const RightSideWrapper = styled(SideWrapper)`
	transform: translate(${FIELD_HEIGHT}cm, ${FIELD_HEIGHT}cm) rotate(270deg);
	transform-origin: 100% 0%;
`;

const BottomSideWrapper = styled(SideWrapper)`
	transform: translate(${FIELD_HEIGHT}cm, ${BOARD_SIZE - FIELD_HEIGHT}cm);
`;

const LeftSideWrapper = styled(SideWrapper)`
	transform: rotate(90deg);
	transform-origin: 0% 100%;
`;

const CornerWrapper = styled.div`
	position: absolute;
	width: ${FIELD_HEIGHT}cm;
	height: ${FIELD_HEIGHT}cm;
	overflow: hidden;
	box-shadow: 0 0 0 1px ${colors.GAME.black}, 0 0 0 1px ${colors.GAME.black} inset;
`;

const TopLeftCornerWrapper = styled(CornerWrapper)`
	transform: translate(0cm, 0cm);
`;

const TopRightCornerWrapper = styled(CornerWrapper)`
	transform: translate(${BOARD_SIZE - FIELD_HEIGHT}cm, 0cm);
`;

const BottomRightCornerWrapper = styled(CornerWrapper)`
	transform: translate(${BOARD_SIZE - FIELD_HEIGHT}cm, ${BOARD_SIZE - FIELD_HEIGHT}cm);
`;

const BottomLeftCornerWrapper = styled(CornerWrapper)`
	bottom: 0;
	left: 0;
`;

const BackgroundWrapper = styled.div`
	transform: translate(${FIELD_HEIGHT}cm, ${FIELD_HEIGHT}cm);
`;

export {
	BoardBleed,
	BoardWrapper,
	TopSideWrapper,
	RightSideWrapper,
	BottomSideWrapper,
	LeftSideWrapper,
	TopLeftCornerWrapper,
	TopRightCornerWrapper,
	BottomRightCornerWrapper,
	BottomLeftCornerWrapper,
	BackgroundWrapper,
};
