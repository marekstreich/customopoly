import React, { FunctionComponent } from 'react';
import { BoardProps } from '../../types/components/Board';
import { getFieldPropsArray } from '../../utilities/methods';
import BoardSide from './BoardSide/BoardSide';
import Background from './Background/Background';
import {
	FreeParking,
	GoToJail,
	Start,
	Jail,
} from './BoardCorners';
import {
	BoardBleed,
	BoardWrapper,
	TopSideWrapper,
	RightSideWrapper,
	BottomSideWrapper,
	LeftSideWrapper,
	TopLeftCornerWrapper,
	TopRightCornerWrapper,
	BottomRightCornerWrapper,
	BottomLeftCornerWrapper,
	BackgroundWrapper,
} from './Board.styled';

export const BoardWithoutBleeds: FunctionComponent<BoardProps> = ({ town }) => {
	return (
		<BoardWrapper>
			<TopLeftCornerWrapper>
				<FreeParking />
			</TopLeftCornerWrapper>
			<TopRightCornerWrapper>
				<GoToJail />
			</TopRightCornerWrapper>
			<BottomRightCornerWrapper>
				<Start />
			</BottomRightCornerWrapper>
			<BottomLeftCornerWrapper>
				<Jail />
			</BottomLeftCornerWrapper>
			<TopSideWrapper>
				<BoardSide fields={getFieldPropsArray({ start: 22, end: 30, town })} />
			</TopSideWrapper>
			<RightSideWrapper>
				<BoardSide fields={getFieldPropsArray({ start: 32, end: 40, town })} />
			</RightSideWrapper>
			<BottomSideWrapper>
				<BoardSide fields={getFieldPropsArray({ start: 2, end: 10, town })} />
			</BottomSideWrapper>
			<LeftSideWrapper>
				<BoardSide fields={getFieldPropsArray({ start: 12, end: 20, town })} />
			</LeftSideWrapper>
			<BackgroundWrapper>
				<Background town={town} />
			</BackgroundWrapper>
		</BoardWrapper>
	);
}

const BoardWithBleeds: FunctionComponent<BoardProps> = ({ town }) => {
	return (
		<BoardBleed>
			<BoardWithoutBleeds town={town} />
		</BoardBleed>
	);
}

export default BoardWithBleeds;
