import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../../utilities/styles';
import { FIELD_WIDTH } from '../Board.constants';

const DISTRICT_NAME_BAR_SIZE = 1;
const DISTRICT_NAME_SIZE = 0.6;

const Name = styled.h6`
	position: absolute;
	top: 1px;
	left: 1px;
	margin: 0;
	line-height: ${DISTRICT_NAME_BAR_SIZE}cm;
	font-family: ${fontFamilies.dosis};
	font-weight: 400;
	font-size: ${DISTRICT_NAME_SIZE}cm;
	text-align: center;
	text-transform: uppercase;
	color: ${colors.GAME.white};
	text-shadow: ${DISTRICT_NAME_SIZE / 20}cm ${DISTRICT_NAME_SIZE / 20}cm 0px rgba(0, 0, 0, 0.2);
	z-index: 2;
`;

const Yellow = styled(Name)`
	width: ${FIELD_WIDTH * 3}cm;
	transform: translate(${FIELD_WIDTH * 6}cm, ${FIELD_WIDTH * 9 - DISTRICT_NAME_BAR_SIZE}cm);
`;

const Orange = styled(Name)`
	width: ${FIELD_WIDTH * 4}cm;
	transform: translate(0cm, ${FIELD_WIDTH * 9 - DISTRICT_NAME_BAR_SIZE}cm);
`;

const Red = styled(Name)`
	width: ${FIELD_WIDTH * 4}cm;
	transform-origin: 0% 100%;
	transform: rotate(90deg) translate(${FIELD_WIDTH * 5 - DISTRICT_NAME_BAR_SIZE}cm, 0cm);
`;

const Pink = styled(Name)`
	width: ${FIELD_WIDTH * 4}cm;
	transform-origin: 0% 100%;
	transform: rotate(90deg) translate(-${DISTRICT_NAME_BAR_SIZE}cm, 0cm);
`;

const Purple = styled(Name)`
	width: ${FIELD_WIDTH * 4}cm;
	transform-origin: 0% 100%;
	transform: rotate(180deg) translate(-${FIELD_WIDTH * 4}cm, ${DISTRICT_NAME_BAR_SIZE}cm);
`;

const Blue = styled(Name)`
	width: ${FIELD_WIDTH * 4}cm;
	transform-origin: 0% 100%;
	transform: rotate(180deg) translate(-${FIELD_WIDTH * 9}cm, ${DISTRICT_NAME_BAR_SIZE}cm);
`;

const Teal = styled(Name)`
	width: ${FIELD_WIDTH * 4}cm;
	transform-origin: 0% 100%;
	transform: rotate(270deg) translate(-${FIELD_WIDTH * 4 - DISTRICT_NAME_BAR_SIZE}cm, ${FIELD_WIDTH * 9}cm);
`;

const Green = styled(Name)`
	width: ${FIELD_WIDTH * 3}cm;
	transform-origin: 0% 100%;
	transform: rotate(270deg) translate(-${FIELD_WIDTH * 9 - DISTRICT_NAME_BAR_SIZE}cm, ${FIELD_WIDTH * 9}cm);
`;

export {
	Yellow,
	Orange,
	Red,
	Pink,
	Purple,
	Blue,
	Teal,
	Green,
};
