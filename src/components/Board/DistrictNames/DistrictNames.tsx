import React, { FunctionComponent } from 'react';
import { BoardProps } from '../../../types/components/Board';
import { DistrictColor } from '../../../types/district';
import {
	Yellow,
	Orange,
	Red,
	Pink,
	Purple,
	Blue,
	Teal,
	Green,
} from './DistrictNames.styled';

const DistrictNames: FunctionComponent<BoardProps> = ({ town }) => {
	return (
		<>
			<Yellow>{town.districts.find(({ color }) => color === DistrictColor.yellow)?.name}</Yellow>
			<Orange>{town.districts.find(({ color }) => color === DistrictColor.orange)?.name}</Orange>
			<Red>{town.districts.find(({ color }) => color === DistrictColor.red)?.name}</Red>
			<Pink>{town.districts.find(({ color }) => color === DistrictColor.pink)?.name}</Pink>
			<Purple>{town.districts.find(({ color }) => color === DistrictColor.purple)?.name}</Purple>
			<Blue>{town.districts.find(({ color }) => color === DistrictColor.blue)?.name}</Blue>
			<Teal>{town.districts.find(({ color }) => color === DistrictColor.teal)?.name}</Teal>
			<Green>{town.districts.find(({ color }) => color === DistrictColor.green)?.name}</Green>
		</>
	);
}

export default DistrictNames;
