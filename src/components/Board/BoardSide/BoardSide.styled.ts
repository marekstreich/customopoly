import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';
import {
	FIELD_WIDTH,
	FIELD_HEIGHT,
	FIELDS_ON_SIDE,
} from './../Board.constants';

const Wrapper = styled.div`
	position: relative;
	width: ${FIELD_WIDTH * FIELDS_ON_SIDE}cm;
	height: ${FIELD_HEIGHT}cm;
	display: flex;
	flex-direction: row-reverse;
	justify-content: flex-start;
	align-items: center;
	box-shadow: 0 0 0 1px ${colors.GAME.black};
`;

export {
	Wrapper,
};
