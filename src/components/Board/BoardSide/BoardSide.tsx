import React, { FunctionComponent } from 'react';
import { BoardSideProps } from '../../../types/components/Board';
import Field from '../BoardFields/Field';
import { Wrapper } from './BoardSide.styled';

const BoardSide: FunctionComponent<BoardSideProps> = ({
	fields = [],
}) => {
	return (
		<Wrapper>
			{fields.map((field) => <Field key={field.number} {...field} />)}
		</Wrapper>
	);
}

export default BoardSide;
