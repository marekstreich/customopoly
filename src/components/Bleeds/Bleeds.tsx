import React, { FunctionComponent } from 'react';
import { BleedProps, BleedColor } from '../../types/components/Bleeds';
import { BLEED_SIZE } from './Bleeds.constants';
import { TopLeft, TopRight, BottomRight, BottomLeft } from './Bleeds.styled';

const Bleeds: FunctionComponent<BleedProps> = ({ color = BleedColor.dark, size = BLEED_SIZE }) => {
	return (
		<>
			<TopLeft color={color} size={size} />
			<TopRight color={color} size={size} />
			<BottomRight color={color} size={size} />
			<BottomLeft color={color} size={size} />
		</>
	);
}

export default Bleeds;
