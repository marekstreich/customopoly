import styled from '@emotion/styled';
import { BleedColor, BleedProps } from '../../types/components/Bleeds';
import { BLEED_SIZE } from './Bleeds.constants';

const getBleedBackground = ({
	color,
}: {
	color?: BleedColor;
}) => color === BleedColor.dark
	? 'rgba(0, 0, 0, 0.15)'
	: 'rgba(255, 255, 255, 0.15)';

const Bleed = styled.div<BleedProps>`
	position: absolute;
	width: ${({ size }) => size || BLEED_SIZE}cm;
	height: ${({ size }) => size || BLEED_SIZE}cm;
	z-index: 1;

	&:before {
		content: '';
		position: absolute;
		width: 1px;
		height: ${({ size }) => (size || BLEED_SIZE) * 1.5}cm;
		background: ${getBleedBackground};
	}

	&:after {
		content: '';
		position: absolute;
		width: ${({ size }) => (size || BLEED_SIZE) * 1.5}cm;
		height: 1px;
		background: ${getBleedBackground};
	}
`;

const TopLeft = styled(Bleed)`
	top: 0;
	left: 0;

	&:before {
		top: 0;
		right: 0;
	}

	&:after {
		bottom: 0;
		left: 0;
	}
`;

const TopRight = styled(Bleed)`
	top: 0;
	right: 0;

	&:before {
		top: 0;
		left: 0;
	}

	&:after {
		bottom: 0;
		right: 0;
	}
`;

const BottomRight = styled(Bleed)`
	bottom: 0;
	right: 0;

	&:before {
		bottom: 0;
		left: 0;
	}

	&:after {
		top: 0;
		right: 0;
	}
`;

const BottomLeft = styled(Bleed)`
	bottom: 0;
	left: 0;

	&:before {
		bottom: 0;
		right: 0;
	}

	&:after {
		top: 0;
		left: 0;
	}
`;

export { TopLeft, TopRight, BottomRight, BottomLeft };
