import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../utilities/styles';
import { BanknoteProps } from '../../types/components/Banknote';
import { BANKNOTE_VALUE_COLOR_MAP } from '../../types/banknote';
import { BLEED_SIZE } from '../Bleeds/Bleeds.constants';
import { BANKNOTE_WIDTH, BANKNOTE_HEIGHT } from './Banknote.constants';
import guiloche from './guiloche.svg';

const SOLID_OUTER_BORDER_SIZE = 0.2;
const SOLID_INNER_BORDER_SIZE = 0.3;
const VALUE_SIZE = 0.8;
const CURRENCY_SIZE = 0.5;
const CURRENCY_VERTICAL_PADDING = 0.1;
const CURRENCY_HORIZONTAL_PADDING = 0.3;
const GRADIENT_INTENSITY = 0.3;

const Wrapper = styled.div<BanknoteProps>`
	position: relative;
	width: ${2 * BLEED_SIZE + BANKNOTE_WIDTH}cm;
	height: ${2 * BLEED_SIZE + BANKNOTE_HEIGHT}cm;
	font-family: ${fontFamilies.dosis};
	background: ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]};
`;

const InnerWrapper = styled.div`
	position: absolute;
	top: ${BLEED_SIZE + SOLID_OUTER_BORDER_SIZE}cm;
	right: ${BLEED_SIZE + SOLID_OUTER_BORDER_SIZE}cm;
	bottom: ${BLEED_SIZE + SOLID_OUTER_BORDER_SIZE}cm;
	left: ${BLEED_SIZE + SOLID_OUTER_BORDER_SIZE}cm;
`;

const Content = styled(InnerWrapper)<BanknoteProps>`
	z-index: 4;
	display: flex;
	justify-content: space-around;
	align-items: center;
`;

const Currency = styled.div<BanknoteProps>`
	font-weight: 700;
	font-size: ${CURRENCY_SIZE}cm;
	line-height: ${CURRENCY_SIZE}cm;
	color: ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]};
	padding: ${CURRENCY_VERTICAL_PADDING}cm ${CURRENCY_HORIZONTAL_PADDING}cm ${CURRENCY_VERTICAL_PADDING * 1.3}cm;
	border-radius: 50%;
	background: ${colors.GAME.white};
	box-shadow: 0 0 0 1px ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]};
`;

const Value = styled.div<BanknoteProps>`
	font-weight: 700;
	font-size: ${VALUE_SIZE}cm;
	line-height: ${VALUE_SIZE}cm;
	color: ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]};
`;

const Frames = styled(InnerWrapper)<BanknoteProps>`
	z-index: 3;
	box-shadow:
		0 0 0 2px white inset,
		0 0 0 4px ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]} inset;

	&:before {
		content: '';
		position: absolute;
		top: ${SOLID_INNER_BORDER_SIZE}cm;
		right: ${SOLID_INNER_BORDER_SIZE}cm;
		bottom: ${SOLID_INNER_BORDER_SIZE}cm;
		left: ${SOLID_INNER_BORDER_SIZE}cm;
		box-shadow:
			0 0 0 1px ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]} inset,
			0 0 0 2px ${colors.GAME.white} inset,
			0 0 0 3px ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]} inset,
			0 0 0 4px ${colors.GAME.white} inset,
			0 0 0 5px ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]} inset;
	}
`;

const SeamlessBackground = styled(InnerWrapper)<BanknoteProps>`
	z-index: 2;
	background: ${colors.GAME.white};

	&:before {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: ${({ value }) => BANKNOTE_VALUE_COLOR_MAP[value]};
		mask-image: url(${guiloche});
		mask-position: center;
		mask-size: 140% auto;
	}
`;

const Gradient = styled.div`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: linear-gradient(25deg, rgba(0, 0, 0, ${GRADIENT_INTENSITY}), rgba(255, 255, 255, ${GRADIENT_INTENSITY}));
	z-index: 1;
`;

export {
	Wrapper,
	Content,
	Currency,
	Value,
	Frames,
	SeamlessBackground,
	Gradient,
};
