import React, { FunctionComponent, useContext } from 'react';
import { CurrencyCode, SUPPORTED_CURRENCIES } from '../../types/currency';
import { BanknoteProps } from '../../types/components/Banknote';
import Bleeds from '../Bleeds/Bleeds';
import CurrencyContext from '../Currency/CurrencyContext';
import {
	Wrapper,
	Content,
	Currency,
	Value,
	Frames,
	SeamlessBackground,
	Gradient,
} from './Banknote.styled';
import { BleedColor } from '../../types/components/Bleeds';

const Banknote: FunctionComponent<BanknoteProps> = ({ value }) => {
	const currencyCode: CurrencyCode = useContext(CurrencyContext);
	const currency = SUPPORTED_CURRENCIES[currencyCode];

	return (
		<Wrapper value={value}>
			<Bleeds color={value === 1000 ? BleedColor.light : BleedColor.dark} />
			<Content value={value}>
				<Currency value={value}>{currency.symbol}</Currency>
				<Value value={value}>{value}</Value>
				<Currency value={value}>{currency.symbol}</Currency>
			</Content>
			<Frames value={value} />
			<SeamlessBackground value={value} />
			<Gradient />
		</Wrapper>
	);
}

export default Banknote;
