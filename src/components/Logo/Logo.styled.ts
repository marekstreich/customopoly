import styled from '@emotion/styled';
import { colors } from '../../utilities/styles';

const LOGO_HEIGHT = 3;
const TITLE_SIZE = 1.15;

const Wrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`;

const Image = styled.img`
	width: auto;
	height: ${LOGO_HEIGHT}rem;
`;

const Title = styled.h1`
	margin: 0;
	font-size: ${TITLE_SIZE}rem;
	line-height: ${TITLE_SIZE}rem;
	text-transform: uppercase;
	letter-spacing: 4px;
	background: linear-gradient(90deg, ${colors.JET.a400}, ${colors.MINT.a600});
	-webkit-background-clip: text;
	-webkit-text-fill-color: transparent;
`;

export {
	Wrapper,
	Image,
	Title,
}