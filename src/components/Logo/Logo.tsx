import React, { FunctionComponent } from 'react';
import { Wrapper, Image, Title } from './Logo.styled';
import logo from './logo.png';

const Logo: FunctionComponent = () => {
	return (
		<Wrapper>
			<Image src={logo} alt="Logo" />
			<Title>Customopoly</Title>
		</Wrapper>
	);
}

export default Logo;
