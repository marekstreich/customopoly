import styled from '@emotion/styled';
import { ButtonProps } from '../../types/components/ButtonProps';
import { colors, fontFamilies } from '../../utilities/styles';

const Button = styled.button<ButtonProps>`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5rem 1rem;
  border-radius: 100rem;
  border: none;
  outline: none;
  font-size: 1rem;
  font-weight: 700;
  font-family: ${fontFamilies.nunito};
  text-transform: uppercase;
  color: ${colors.WHITE.a025};
  background: ${props => props.color === 'primary' ? colors.MINT.a600 : colors.WENGE.a600};
  cursor: pointer;
  transition: color 0.2s, background 0.2s;

  &:hover, &:focus {
    background: ${props => props.color === 'primary' ? colors.MINT.a500 : colors.WENGE.a500};
    transition: background 0.2s;
  }

  svg {
    margin-right: 0.5rem;
  }
`

export default Button;
