import styled from '@emotion/styled';
import { colors } from '../../utilities/styles';

const VIEW_SPACING = 2;

const Section = styled.section`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
`;

const Title = styled.h1`
	align-self: flex-start;
	margin: ${VIEW_SPACING}rem;
	padding: 0;
	font-size: 2rem;
	letter-spacing: 4px;
	background: linear-gradient(90deg, ${colors.WENGE.a500}, ${colors.JET.a200});
	-webkit-background-clip: text;
	-webkit-text-fill-color: transparent;
	border-bottom: 3px solid ${colors.WENGE.a050};
`;

const ContentWrapper = styled.div`
	flex: 1;
	align-self: stretch;
	overflow: auto;
`;

const Content = styled.div`
	display: inline-flex;
	overflow: hidden;
	padding: 0 ${VIEW_SPACING}rem ${VIEW_SPACING}rem;
`;

export {
	Section,
	Title,
	ContentWrapper,
	Content,
};
