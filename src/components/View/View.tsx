import React, { FunctionComponent } from 'react';
import { Section, Title, ContentWrapper, Content } from './View.styled';

export const ViewTitle: FunctionComponent = ({ children }) => {
	return (
		<Title>{children}</Title>
	);
};

export const ViewContent: FunctionComponent = ({ children }) => {
	return (
		<ContentWrapper>
			<Content>{children}</Content>
		</ContentWrapper>
	);
};

const View: FunctionComponent = ({ children }) => {
	return (
		<Section>{children}</Section>
	);
};

export default View;
