import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../../utilities/styles';

const TITLE_SIZE = 2;
const SUBTITLE_SIZE = 0.5;

const Heading = styled.h4`
	text-transform: uppercase;
	font-family: ${fontFamilies.dosis};
	color: ${colors.GAME.white};
	text-align: center;
`;

const Title = styled(Heading)`
	margin: 0;
	line-height: ${TITLE_SIZE}cm;
	font-size: ${TITLE_SIZE}cm;
	font-weight: 700;
	text-shadow: ${TITLE_SIZE / 20}cm ${TITLE_SIZE / 20}cm 0cm rgba(0, 0, 0, 0.2);
`;

const Subtitle = styled(Heading)`
	margin: ${SUBTITLE_SIZE / 2}cm 0 0;
	line-height: ${SUBTITLE_SIZE}cm;
	font-size: ${SUBTITLE_SIZE}cm;
	font-weight: 400;
	text-shadow: ${SUBTITLE_SIZE / 20}cm ${SUBTITLE_SIZE / 20}cm 0cm rgba(0, 0, 0, 0.2);
`;

export {
	Title,
	Subtitle,
};
