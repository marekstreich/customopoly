import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../../utilities/styles';

const TITLE_SIZE = 0.8;
const SUBTITLE_SIZE = 0.3;

const Heading = styled.h4`
	text-transform: uppercase;
	font-family: ${fontFamilies.dosis};
`;

const Title = styled(Heading)`
	margin: 0;
	line-height: ${TITLE_SIZE}cm;
	font-size: ${TITLE_SIZE}cm;
	font-weight: 700;
	color: ${colors.GAME.red};
`;

const Subtitle = styled(Heading)`
	margin: ${SUBTITLE_SIZE / 2}cm 0 0;
	line-height: ${SUBTITLE_SIZE}cm;
	font-size: ${SUBTITLE_SIZE}cm;
	font-weight: 400;
	color: ${colors.GAME.gray};
`;

export {
	Title,
	Subtitle,
};
