import React, { FunctionComponent } from 'react';
import { TitleProps } from '../../../types/components/Common';
import { Title, Subtitle } from './SmallTitle.styled';

const SmallTitle: FunctionComponent<TitleProps> = ({ town, hideSubtitle }) => {
	return (
		<>
			<Title>{town.meta.title || `${town.name} Biznes`}</Title>
			{town.meta.subtitle && !hideSubtitle && <Subtitle>{town.meta.subtitle}</Subtitle>}
		</>
	);
}

export default SmallTitle;
