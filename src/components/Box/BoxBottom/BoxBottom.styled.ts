import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../../utilities/styles';
import { BOX_SIZE_X, BOX_SIZE_Y, BOX_SPACING } from '../Box.constants';

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;
	width: ${BOX_SIZE_X - 2 * BOX_SPACING}cm;
	height: ${BOX_SIZE_Y - 2 * BOX_SPACING}cm;
	padding: ${BOX_SPACING}cm;
	background: ${colors.GAME.white};
	font-family: ${fontFamilies.dosis};
`;

export {
	Wrapper,
};
