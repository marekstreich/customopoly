import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../types/components/Box';
import { BleedsWrapper } from '../Box.styled';
import BoxBottomContent from './BoxBottomContent/BoxBottomContent';
import BoxBottomNotes from './BoxBottomNotes/BoxBottomNotes';
import { Wrapper } from './BoxBottom.styled';
import Bleeds from '../../Bleeds/Bleeds';
import { BOX_BLEED_SIZE } from '../Box.constants';

const BoxBottom: FunctionComponent<BoxProps> = ({ town }) => {
	return (
		<BleedsWrapper>
			<Bleeds size={BOX_BLEED_SIZE} />
			<Wrapper>
				<BoxBottomContent town={town} />
				<BoxBottomNotes town={town} />
			</Wrapper>
		</BleedsWrapper>
	);
}

export default BoxBottom;
