import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../../types/components/Box';
import { StreetFieldNumber } from '../../../../types/field';
import { getStreetWithDistrictByFieldNumber } from '../../../../utilities/methods';
import { BoardWithoutBleeds } from '../../../Board/Board';
import CardProperty from '../../../Card/CardProperty/CardProperty';
import BigTitle from '../../../Common/BigTitle/BigTitle';
import {
	Wrapper,
	TitleWrapper,
	TextWrapper,
	Text,
	CardsWrapper,
	CardWrapper,
	Card,
	ElementsWrapper,
	Elements,
	Element,
	BoardWrapper,
	BoardPlacer,
} from './BoxBottomContent.styled';

const STREET_FIELD_NUMBERS: StreetFieldNumber[] = [12, 30, 35, 38];

const BoxBottomContent: FunctionComponent<BoxProps> = ({ town }) => {
	const streets = STREET_FIELD_NUMBERS.map((number) => getStreetWithDistrictByFieldNumber({ number, town }));
	return (
		<Wrapper>
			<TitleWrapper>
				<BigTitle town={town} />
			</TitleWrapper>
			<TextWrapper>
				<Text>Możliwe, że już od dziś Stargard będzie należeć do Ciebie! Jeśli się postarasz zostaniesz milionerem i będziesz władać najważniejszymi ulicami. Jednak zachowaj ostrożność! Są&nbsp;ludzie, którzy czyhają na Twoje pieniądze. Tylko od Ciebie zależy czy będziesz posiadać największy majątek czy zostaniesz po prostu menelem spod Darkatu...</Text>
			</TextWrapper>
			<CardsWrapper>
				{streets.map((street) => street && <CardWrapper key={street.number}>
					<Card>
						<CardProperty
							number={street.number}
							name={street.name}
							district={street.district}
							color={street.color}
							price={town.price[street.color][street.type]}
							charge={town.charge[street.color][street.type]}
						/>
					</Card>
				</CardWrapper>)}
			</CardsWrapper>
			<ElementsWrapper>
				<Elements>
					<Element><strong>elementy gry:</strong></Element>
					<Element style={{ display: 'none' }}>instrukcja</Element>
					<Element>plansza</Element>
					<Element>5 pionków</Element>
					<Element>22 karty ulic</Element>
					<Element>2 karty firm</Element>
					<Element>4 karty pętli komunikacji miejskiej</Element>
					<Element>64 karty niespodzianki</Element>
					<Element>358 banknotów</Element>
					<Element>32 domki</Element>
					<Element>12 hoteli</Element>
				</Elements>
			</ElementsWrapper>
			<BoardWrapper>
				<BoardPlacer>
					<BoardWithoutBleeds town={town} />
				</BoardPlacer>
			</BoardWrapper>
		</Wrapper>
	);
}

export default BoxBottomContent;
