import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { shadowPrintFix } from '../../../../utilities/styles/mixins';
import { BOARD_SIZE } from '../../../Board/Board.constants';
import { CARD_HEIGHT, CARD_WIDTH } from '../../../Card/Card.constants';
import { BOX_SIZE_X, BOX_SIZE_Y, BOX_SPACING, BORDER_RADIUS } from '../../Box.constants';

const CONTENT_HEIGHT_RATIO = 0.85;
const TITLE_WRAPPER_HEIGHT = 3;
const CARDS_WRAPPER_HEIGHT = 4;
const CARDS_COUNT = 4;
const CARDS_SCALE_RATIO = Math.round(CARDS_WRAPPER_HEIGHT / CARD_HEIGHT * 10) / 10;
const CARDS_SPACING = BOX_SPACING * -0.5;
const CARDS_WRAPPER_WIDTH = CARD_WIDTH * CARDS_SCALE_RATIO * CARDS_COUNT + (CARDS_COUNT - 1) * CARDS_SPACING;
const CARDS_TRANSFORM_OFFSET = CARDS_SCALE_RATIO * 100 / 2;
const CARDS_ROTATE_X = 22.5;
const CARDS_ROTATE_Y = -15;
const BOARD_WRAPPER_WIDTH = 16;
const BOARD_WRAPPER_HEIGHT = 8;
const BOARD_DESIRED_SIZE = 10.5;
const BOARD_SCALE_RATIO = Math.round(BOARD_DESIRED_SIZE / BOARD_SIZE * 100) / 100;
const BOARD_TRANSFORM_OFFSET_X = (BOARD_SIZE - BOARD_WRAPPER_WIDTH) / 2;
const BOARD_TRANSFORM_OFFSET_Y = (BOARD_SIZE - BOARD_WRAPPER_HEIGHT) / 2;
const BOARD_ROTATE_X = 60;
const BOARD_ROTATE_Z = 30;

const Wrapper = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	width: ${BOX_SIZE_X - 2 * BOX_SPACING}cm;
	height: ${BOX_SIZE_Y * CONTENT_HEIGHT_RATIO - 2 * BOX_SPACING}cm;
	margin-bottom: ${BOX_SPACING * 0.5}cm;
	background: linear-gradient(
		45deg,
		${colors.GAME.yellow},
		${colors.GAME.orange},
		${colors.GAME.red},
		${colors.GAME.pink}
	);
	border-radius: ${BORDER_RADIUS}cm;
`;

const ContentPartWrapper = styled.div`
	box-sizing: border-box;
	position: absolute;
	z-index: 1;
`;

const TitleWrapper = styled(ContentPartWrapper)`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	top: ${BOX_SPACING}cm;
	right: ${BOX_SPACING}cm;
	height: ${TITLE_WRAPPER_HEIGHT}cm;
	left: ${BOX_SPACING}cm;
`;

const TextWrapper = styled(ContentPartWrapper)`
	top: ${BOX_SPACING * 2 + TITLE_WRAPPER_HEIGHT}cm;
	right: ${BOX_SPACING}cm;
	left: ${BOX_SPACING * 1.5 + CARDS_WRAPPER_WIDTH}cm;
`;

const Text = styled.p`
	margin: 0;
	padding: ${BOX_SPACING * 0.5}cm;
	background: linear-gradient(
		45deg,
		${colors.GAME.red},
		${colors.GAME.pink}
	);
	font-size: 0.45cm;
	text-align: justify;
	color: ${colors.GAME.white};
	border-radius: ${BORDER_RADIUS * 0.5}cm;
	box-shadow: 0 0 ${BOX_SPACING * 0.5}cm rgba(0, 0, 0, 0.2);
	${shadowPrintFix}
`;

const CardsWrapper = styled(ContentPartWrapper)`
	top: ${BOX_SPACING * 2 + TITLE_WRAPPER_HEIGHT}cm;
	width: ${CARDS_WRAPPER_WIDTH}cm;
	height: ${CARDS_WRAPPER_HEIGHT}cm;
	left: ${BOX_SPACING}cm;
	padding: 0;
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
	z-index: 2;
`;

const CardWrapper = styled.div`
	width: ${CARD_WIDTH * CARDS_SCALE_RATIO}cm;
	height: ${CARD_HEIGHT * CARDS_SCALE_RATIO}cm;
	margin-right: ${CARDS_SPACING}cm;
`;

const Card = styled.div`
	overflow: hidden;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-shrink: 0;
	width: ${CARD_WIDTH}cm;
	height: ${CARD_HEIGHT}cm;
	box-shadow: 0 0 1cm rgba(0, 0, 0, 0.3);
	transform:
		translate(-${CARDS_TRANSFORM_OFFSET}%, -${CARDS_TRANSFORM_OFFSET}%)
		scale(${CARDS_SCALE_RATIO})
		rotateX(${CARDS_ROTATE_X}deg)
		rotateY(${CARDS_ROTATE_Y}deg)
		perspective(10cm);
	transform-origin: center;
	${shadowPrintFix}
`;

const ElementsWrapper = styled(ContentPartWrapper)`
	top: ${BOX_SPACING * 3 + TITLE_WRAPPER_HEIGHT + CARDS_WRAPPER_HEIGHT}cm;
	right: ${BOX_SPACING * 1.5 + BOARD_WRAPPER_WIDTH}cm;
	bottom: ${BOX_SPACING}cm;
	left: ${BOX_SPACING}cm;
`;

const Elements = styled.ul`
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;
	margin: 0;
	padding: 0;
	list-style-type: none;
`;

const Element = styled.li`
	margin: 0;
	padding: 0;
	line-height: 0.45cm;
	font-size: 0.45cm;
	color: ${colors.GAME.white};
	text-shadow: 0.03cm 0.03cm 0 rgba(0, 0, 0, 0.2);
`;

const BoardWrapper = styled(ContentPartWrapper)`
	height: ${BOARD_WRAPPER_HEIGHT}cm;
	right: ${BOX_SPACING}cm;
	bottom: ${BOX_SPACING}cm;
	width: ${BOARD_WRAPPER_WIDTH}cm;
	padding: 0;
`;

const BoardPlacer = styled.div`
	width: ${BOARD_SIZE}cm;
	height: ${BOARD_SIZE}cm;
	transform:
		translate(-${BOARD_TRANSFORM_OFFSET_X}cm, -${BOARD_TRANSFORM_OFFSET_Y}cm)
		scale(${BOARD_SCALE_RATIO})
		rotateX(${BOARD_ROTATE_X}deg)
		rotateZ(${BOARD_ROTATE_Z}deg)
		perspective(10cm);
	transform-origin: center;

	& > div {
		border-radius: 0.5cm;
		box-shadow: 0 0 0 1cm ${colors.GAME.white}, 0 0 25cm rgba(0, 0, 0, 0.75);
		${shadowPrintFix}
	}
`;

export {
	Wrapper,
	TitleWrapper,
	TextWrapper,
	Text,
	CardsWrapper,
	CardWrapper,
	Card,
	ElementsWrapper,
	Elements,
	Element,
	BoardWrapper,
	BoardPlacer,
};
