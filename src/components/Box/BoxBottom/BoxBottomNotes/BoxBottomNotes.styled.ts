import styled from '@emotion/styled';
import { colors } from '../../../../utilities/styles';
import { IMAGE_HEIGHT } from '../../Box.constants';

const Wrapper = styled.div`
	flex: 1;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: stretch;
	color: ${colors.GAME.gray};
`;

const Section = styled.section`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: flex-start;
	max-width: 30%;
`;

const MetaList = styled.ul`
	display: flex;
	flex-direction: column;
	flex-direction: flex-start;
	align-items: flex-start;
	margin: 0;
	padding: 0;
	list-style-type: none;
`;

const MetaListItem = styled.li`
	margin: 0;
	padding: 0;
	font-size: 0.25cm;
	font-weight: 400;
	color: ${colors.GAME.black};
`;

const Paragraph = styled.p`
	margin: 0;
	font-size: 0.2cm;
	font-weight: 400;
	color: ${colors.GAME.black};
`;

const Image = styled.img`
	height: ${IMAGE_HEIGHT}cm;
`;

const KidsWarningWrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`;

const NotSafeForKidsImage = styled.img`
	display: block;
	width: 1cm;
	height: 1cm;
`;

export {
	Wrapper,
	Section,
	MetaList,
	MetaListItem,
	Paragraph,
	Image,
	KidsWarningWrapper,
	NotSafeForKidsImage,
};
