import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../../types/components/Box';
import { InfoIconsTheme } from '../../../../types/components/InfoIcons';
import InfoIcons from '../../../InfoIcons/InfoIcons';
import img3 from '../../../../assets/img-3.png';
import notSafeForKids from './assets/not-safe-for-kids.jpg';
import {
	Wrapper,
	Section,
	MetaList,
	MetaListItem,
	Paragraph,
	Image,
	KidsWarningWrapper,
	NotSafeForKidsImage,
} from './BoxBottomNotes.styled';

const BoxBottomNotes: FunctionComponent<BoxProps> = ({ town }) => {
	return (
		<Wrapper>
			<Section>
				<MetaList>
					<MetaListItem>Autorzy gry: Kinga Ziembicka Streich oraz Marek Streich</MetaListItem>
					<MetaListItem>Ilustracje: macrovector / Freepik</MetaListItem>
					<MetaListItem>Projekt graficzny: Marek Streich</MetaListItem>
					<MetaListItem>Prawa autorskie do nazw oraz użytych tekstów: {town.meta.creator}</MetaListItem>
				</MetaList>
				<Paragraph>
					Wyprodukowano w Polsce<br />
					© Kinga Ziembicka Streich oraz Marek Streich 2021
				</Paragraph>
			</Section>
			<Section style={{ justifyContent: 'center' }}>
				<Image src={img3} alt="Biznes" />
			</Section>
			<Section style={{ alignItems: 'stretch' }}>
				<InfoIcons infoIconTheme={InfoIconsTheme.dark} />
				<KidsWarningWrapper>
					<NotSafeForKidsImage src={notSafeForKids} alt="Nieodpowiednie dla dzieci w wieku poniżej 3 lat" />
					<Paragraph style={{ textAlign: 'right' }}>
						<strong>Ostrzeżenie:</strong> Nieodpowiednie dla dzieci w wieku poniżej 3 lat.<br />Gra zawiera małe elementy. Istnieje możliwość zadławienia.<br />Prosimy zachować opakowanie ze względu na podane informacje.
					</Paragraph>
				</KidsWarningWrapper>
			</Section>
		</Wrapper>
	);
}

export default BoxBottomNotes;
