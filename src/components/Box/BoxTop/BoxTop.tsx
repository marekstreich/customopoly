import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../types/components/Box';
import Bleeds from '../../Bleeds/Bleeds';
import BigTitle from '../../Common/BigTitle/BigTitle';
import { BOX_BLEED_SIZE } from '../Box.constants';
import { BleedsWrapper } from '../Box.styled';
import {
	Wrapper,
	Content,
	TitleWrapper,
	BottomImageSpacer,
} from './BoxTop.styled';

const BoxTop: FunctionComponent<BoxProps> = ({ town }) => {
	return (
		<BleedsWrapper>
			<Bleeds size={BOX_BLEED_SIZE} />
			<Wrapper>
				<Content>
					<TitleWrapper>
						<BigTitle town={town} />
					</TitleWrapper>
					<BottomImageSpacer />
				</Content>
			</Wrapper>
		</BleedsWrapper>
	);
}

export default BoxTop;
