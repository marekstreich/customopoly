import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';
import { BOX_SIZE_X, BOX_SIZE_Y, BOX_SPACING, BORDER_RADIUS } from '../Box.constants';
import town from '../../../assets/town.jpg';
import { shadowPrintFix } from '../../../utilities/styles/mixins';

const Wrapper = styled.div`
	display: flex;
	align-items: stretch;
	width: ${BOX_SIZE_X - 2 * BOX_SPACING}cm;
	height: ${BOX_SIZE_Y - 2 * BOX_SPACING}cm;
	padding: ${BOX_SPACING}cm;
	background: ${colors.GAME.white};
`;

const Content = styled.div`
	position: relative;
	overflow: hidden;
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	width: ${BOX_SIZE_X - 4 * BOX_SPACING}cm;
	height: ${BOX_SIZE_Y - 4 * BOX_SPACING}cm;
	padding: ${BOX_SPACING}cm;
	background: ${colors.GAME.sky} url(${town}) bottom center no-repeat;
	background-size: contain;
	border-radius: ${BORDER_RADIUS}cm;
	${shadowPrintFix}

	&:before {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: red;
		background: linear-gradient(
			180deg,
			${colors.GAME.sky} 50%,
			rgba(26, 189, 224, 0) 75%
		);
		${shadowPrintFix}
	}
`;

const TitleWrapper = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	z-index: 1;
`;

const BottomImageSpacer = styled.div`
	height: 35%;
`;

export {
	Wrapper,
	Content,
	TitleWrapper,
	BottomImageSpacer,
};
