import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../types/components/Box';
import { InfoIconsTheme } from '../../../types/components/InfoIcons';
import InfoIcons from '../../InfoIcons/InfoIcons';
import { BleedsWrapper, Image } from '../Box.styled';
import { Wrapper, Typography } from './BoxSide1.styled';
import img2 from '../../../assets/img-2.png';
import SmallTitle from '../../Common/SmallTitle/SmallTitle';
import Bleeds from '../../Bleeds/Bleeds';
import { BOX_BLEED_SIZE } from '../Box.constants';

const BoxSide1: FunctionComponent<BoxProps> = ({ town }) => {
	return (
		<BleedsWrapper>
			<Bleeds size={BOX_BLEED_SIZE} />
			<Wrapper>
				<Typography>
					<SmallTitle town={town} />
				</Typography>
				<Image src={img2} alt="Biznes" />
				<InfoIcons infoIconTheme={InfoIconsTheme.dark} />
			</Wrapper>
		</BleedsWrapper>
	);
}

export default BoxSide1;
