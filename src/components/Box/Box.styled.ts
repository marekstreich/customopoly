import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../utilities/styles';
import { BOX_BLEED_SIZE, IMAGE_HEIGHT } from './Box.constants';

const BleedsWrapper = styled.div`
	position: relative;
	overflow: hidden;
	padding: ${BOX_BLEED_SIZE}cm;
`;

const Title = styled.h2`
	margin: 0;
	text-transform: uppercase;
	font-family: ${fontFamilies.dosis};
	font-size: 0.8cm;
	font-weight: 700;
	background: linear-gradient(
		90deg,
		${colors.GAME.purple},
		${colors.GAME.pink},
		${colors.GAME.red},
		${colors.GAME.orange},
		${colors.GAME.yellow}
	);
	-webkit-background-clip: text;
	-webkit-text-fill-color: transparent;
`;

const Subtitle = styled.h5`
	margin: 0;
	text-transform: uppercase;
	font-family: ${fontFamilies.dosis};
	font-size: 0.3cm;
	font-weight: 400;
	color: ${colors.GAME.gray};
`;

const Image = styled.img`
	height: ${IMAGE_HEIGHT}cm;
`;

export { BleedsWrapper, Title, Subtitle, Image };
