import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../types/components/Box';
import { BleedsWrapper, Image } from '../Box.styled';
import { Wrapper, Typography } from './BoxSide2.styled';
import img1 from '../../../assets/img-1.png';
import img4 from '../../../assets/img-4.png';
import SmallTitle from '../../Common/SmallTitle/SmallTitle';
import Bleeds from '../../Bleeds/Bleeds';
import { BOX_BLEED_SIZE } from '../Box.constants';

const BoxSide2: FunctionComponent<BoxProps> = ({ town }) => {
	return (
		<BleedsWrapper>
			<Bleeds size={BOX_BLEED_SIZE} />
			<Wrapper>
				<Image src={img1} alt="Biznes" />
				<Typography>
					<SmallTitle town={town} />
				</Typography>
				<Image src={img4} alt="Biznes" />
			</Wrapper>
		</BleedsWrapper>
	);
}

export default BoxSide2;
