import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';
import { BOX_SIZE_Y, BOX_SIZE_Z, BOX_SPACING } from '../Box.constants';

const Wrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	width: ${BOX_SIZE_Y - 2 * BOX_SPACING}cm;
	height: ${BOX_SIZE_Z - 2 * BOX_SPACING}cm;
	padding: ${BOX_SPACING}cm;
	background: ${colors.GAME.white};
`;

const Typography = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
`;

export { Wrapper, Typography };
