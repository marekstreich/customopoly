import React, { FunctionComponent } from 'react';
import { BoxProps } from '../../../types/components/Box';
import BoxSide1 from '../BoxSide1/BoxSide1';
import BoxSide2 from '../BoxSide2/BoxSide2';

const BoxSides: FunctionComponent<BoxProps> = ({ town }) => {
	return (
		<>
			<BoxSide1 town={town} />
			<BoxSide2 town={town} />
			<BoxSide1 town={town} />
			<BoxSide2 town={town} />
		</>
	);
}

export default BoxSides;
