import React, { FunctionComponent, useContext } from 'react';
import { CurrencyProps } from '../../types/components/Currency';
import { CurrencyCode, SUPPORTED_CURRENCIES } from '../../types/currency';
import CurrencyContext from './CurrencyContext';


const Currency: FunctionComponent<CurrencyProps> = ({ amount }) => {
	const currencyCode: CurrencyCode = useContext(CurrencyContext);
	const currency = SUPPORTED_CURRENCIES[currencyCode];
	const symbolMargin = currency.space ? '0.2em' : '0';

	return (
		<span>
			{currency.position === 'left' && <span style={{
				marginRight: symbolMargin,
				textTransform: currency.case || 'none',
			}}>{currency.symbol}</span>}
			<span>{amount}</span>
			{currency.position === 'right' && <span style={{
				marginLeft: symbolMargin,
				textTransform: currency.case || 'none',
			}}>{currency.symbol}</span>}
		</span>
	);
}

export default Currency;
