
import React from 'react';
import { CurrencyCode } from '../../types/currency';

const CurrencyContext = React.createContext(CurrencyCode.USD);

export default CurrencyContext;