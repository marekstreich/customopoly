import React, { FunctionComponent } from 'react';
import { CardChanceProps } from '../../../types/components/Card';
import { CHANCE_TYPE_ICON_MAP } from '../../../types/chance';
import Bleeds from '../../Bleeds/Bleeds';
import { Wrapper, Icons, IconsRow, Icon, Content, Text } from './CardChance.styled';
import { ICON_ROWS, ICONS_IN_ROW } from './CardChance.constants';

const CardChance: FunctionComponent<CardChanceProps> = ({ text, type, showBleeds = true }) => {
	const icon = CHANCE_TYPE_ICON_MAP[type];

	return (
		<Wrapper type={type}>
			{showBleeds && <Bleeds />}
			<Icons>
				{Array.from(Array(ICON_ROWS).keys()).map((key) => <IconsRow key={key}>
					{Array.from(Array(ICONS_IN_ROW).keys()).map((key) => <Icon key={key}>
						{icon}
					</Icon>)}
				</IconsRow>)}
			</Icons>
			{text && <Content type={type}>
				<Text type={type}>{text}</Text>
			</Content>}
		</Wrapper>
	);
}

export default CardChance;
