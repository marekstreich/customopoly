import styled from '@emotion/styled';
import { CardChanceType } from '../../../types/components/Card';
import { colors, fontFamilies } from '../../../utilities/styles';
import { BLEED_SIZE } from '../../Bleeds/Bleeds.constants';
import { CARD_WIDTH, CARD_HEIGHT } from '../Card.constants';
import { CHANE_TYPE_COLOR_MAP } from '../../../types/chance';
import { ICON_ROWS } from './CardChance.constants';

const PADDING = 0.5;
const ICON_SIZE = CARD_WIDTH / ICON_ROWS;
const TEXT_SIZE = 0.5;
const GRADIENT_INTENSITY = 0.3;
const CONTENT_WIDTH = CARD_HEIGHT - BLEED_SIZE - PADDING;
const CONTENT_HEIGHT = CARD_WIDTH - BLEED_SIZE - PADDING;

const Wrapper = styled.div<CardChanceType>`
	position: relative;
	width: ${BLEED_SIZE * 2 + CARD_HEIGHT}cm;
	height: ${BLEED_SIZE * 2 + CARD_WIDTH}cm;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	align-items: stretch;
	background: ${({ type }) => CHANE_TYPE_COLOR_MAP[type]};
	font-family: ${fontFamilies.dosis};

	&:before {
		content: '';
		position: absolute;
		top: 0px;
		right: 0px;
		bottom: 0px;
		left: 0px;
		background: linear-gradient(
			45deg,
			rgba(255, 255, 255, ${GRADIENT_INTENSITY}),
			rgba(0, 0, 0, ${GRADIENT_INTENSITY})
		);
	}
`;

const Icons = styled.div`
	position: absolute;
	top: ${BLEED_SIZE}cm;
	right: ${BLEED_SIZE}cm;
	bottom: ${BLEED_SIZE}cm;
	left: ${BLEED_SIZE}cm;
	z-index: 1;
`;

const IconsRow = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	align-items: center;
`;

const Icon = styled.div`
	width: ${ICON_SIZE}cm;
	height: ${ICON_SIZE}cm;
	display: flex;
	justify-content: center;
	align-items: center;

	svg {
		width: ${ICON_SIZE * 0.6}cm;
		height: ${ICON_SIZE * 0.6}cm;
		color: ${colors.GAME.white};
	}
`;

const Content = styled.div<CardChanceType>`
	box-sizing: border-box;
	position: absolute;
	top: 50%;
	width: ${CONTENT_WIDTH}cm;
	height: ${CONTENT_HEIGHT}cm;
	left: 50%;
	margin: ${CONTENT_HEIGHT * -.5}cm 0 0 ${CONTENT_WIDTH * -.5}cm;
	padding: ${PADDING}cm;
	display: flex;
	justify-content: center;
	align-items: center;
	border-radius: 50%;
	color: ${({ type }) => CHANE_TYPE_COLOR_MAP[type]};
	background: ${colors.GAME.white};
	z-index: 2;
`;

const Text = styled.div<CardChanceType>`
	text-align: center;
	font-size: ${TEXT_SIZE}cm;
	font-weight: 700;
	text-transform: uppercase;
	color: ${({ type }) => CHANE_TYPE_COLOR_MAP[type]};
`;

export {
	Wrapper,
	Icons,
	IconsRow,
	Icon,
	Content,
	Text,
};
