const ICON_ROWS = 5;
const ICONS_IN_ROW = 7;

export {
	ICON_ROWS,
	ICONS_IN_ROW,
};
