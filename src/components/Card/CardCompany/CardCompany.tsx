import React, { FunctionComponent } from 'react';
import { CardCompanyProps } from '../../../types/components/Card';
import { COMPANY_ICON_MAP, COMPANY_NUMBER_COLOR_MAP } from '../../../types/company';
import Bleeds from '../../Bleeds/Bleeds';
import Currency from '../../Currency/Currency';
import Separator from '../Separator/Separator';
import {
	Meta,
	MetaItem,
	Name,
	Section,
	Row,
	Col,
	ColWithText,
	Value,
 } from '../CardCommon.styled';
import {
	Wrapper,
	Header,
	Icon,
	Content,
} from '../CardSpecial.styled';

const CardCompany: FunctionComponent<CardCompanyProps> = ({
	name,
	number,
	icon,
}) => {
	const iconComponent = COMPANY_ICON_MAP[icon];
	const color = COMPANY_NUMBER_COLOR_MAP[number];

	return (
		<Wrapper>
			<Bleeds />
			<Header color={color}>
				<Meta>
					<MetaItem>{number}</MetaItem>
				</Meta>
				<Name>{name}</Name>
				<Icon>
					{iconComponent}
				</Icon>
			</Header>
			<Content>
				<Section>
					<Row>
						<Col>Cena zakupu</Col>
						<Value>
							<Currency amount={300} />
						</Value>
					</Row>
				</Section>
				<Separator>Opłata za postój</Separator>
				<Section>
					<Row>
						<ColWithText>Opłata wynosi 10 X ilość wyrzuconych oczek.<br />Jeśli właściciel posiada obie firmy to opłata jest podwójna.</ColWithText>
					</Row>
				</Section>
				<Separator />
				<Section>
					<Row>
						<Col>Zastaw hipoteczny</Col>
						<Value>
							<Currency amount={150} />
						</Value>
					</Row>
				</Section>
			</Content>
		</Wrapper>
	);
}

export default CardCompany;
