import React, { FunctionComponent } from 'react';
import { CardPropertyProps } from '../../../types/components/Card';
import { districtOrder } from '../../../types/district';
import { ChargeTypes } from '../../../types/charge';
import Bleeds from '../../Bleeds/Bleeds';
import Currency from '../../Currency/Currency';
import Separator from '../Separator/Separator';
import {
	Meta,
	MetaItem,
	Name,
	Section,
	Row,
	Col,
	Value,
	Text,
 } from '../CardCommon.styled';
import {
	Wrapper,
	Header,
	Content,
 } from './CardProperty.styled';

const CardProperty: FunctionComponent<CardPropertyProps> = ({
	name,
	number,
	district,
	color,
	price,
	charge,
}) => {
	const colorIndex = districtOrder.indexOf(color);
	const quarter = Math.ceil((colorIndex + 1) / 2);
	return (
		<Wrapper>
			<Bleeds />
			<Header color={color}>
				<Meta>
					<MetaItem color={color}>{number}</MetaItem>
					<MetaItem color={color}>{district}</MetaItem>
				</Meta>
				<Name color={color}>{name}</Name>
			</Header>
			<Content>
				<Section>
					<Row>
						<Col>Cena zakupu</Col>
						<Value>
							<Currency amount={price} />
						</Value>
					</Row>
				</Section>
				<Separator>Opłata za postój</Separator>
				<Section>
					<Row>
						<Col>Teren niezabudowany</Col>
						<Value>
							<Currency amount={charge[ChargeTypes.empty]} />
						</Value>
					</Row>
					<Row>
						<Col>Teren z 1 domem</Col>
						<Value>
							<Currency amount={charge[ChargeTypes.house1]} />
						</Value>
					</Row>
					<Row>
						<Col>Teren z 2 domami</Col>
						<Value>
							<Currency amount={charge[ChargeTypes.house2]} />
						</Value>
					</Row>
					<Row>
						<Col>Teren z 3 domami</Col>
						<Value>
							<Currency amount={charge[ChargeTypes.house3]} />
						</Value>
					</Row>
					<Row>
						<Col>Teren z 4 domami</Col>
						<Value>
							<Currency amount={charge[ChargeTypes.house4]} />
						</Value>
					</Row>
					<Row>
						<Col>Teren z hotelem</Col>
						<Value>
							<Currency amount={charge[ChargeTypes.hotel]} />
						</Value>
					</Row>
				</Section>
				<Separator />
				<Text>Jeśli gracz posiada wszystkie tereny budowlane na tym osiedlu i są one niezabudowane to opłata jest podwójna</Text>
				<Separator />
				<Section>
					<Row>
						<Col>1 dom kosztuje</Col>
						<Value>
							<Currency amount={quarter * 100} />
						</Value>
					</Row>
					<Row>
						<Col>Zastaw hipoteczny</Col>
						<Value>
							<Currency amount={price * 0.5} />
						</Value>
					</Row>
				</Section>
			</Content>
		</Wrapper>
	);
}

export default CardProperty;
