import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../../utilities/styles';
import { CardPropertyColor } from '../../../types/components/Card';
import { BLEED_SIZE } from '../../Bleeds/Bleeds.constants';
import { CARD_WIDTH, CARD_HEIGHT, PADDING } from '../Card.constants';

const HEADER_HEIGHT = 2;
const CONTENT_HEIGHT = CARD_HEIGHT - HEADER_HEIGHT - 0.15;
const GRADIENT_INTENSITY = 0.15;

const Wrapper = styled.div`
	position: relative;
	width: ${BLEED_SIZE * 2 + CARD_WIDTH}cm;
	height: ${BLEED_SIZE * 2 + CARD_HEIGHT}cm;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	background: ${colors.GAME.black};
	font-family: ${fontFamilies.dosis};
`;

const Header = styled.div<CardPropertyColor>`
	position: relative;
	box-sizing: border-box;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;
	width: 100%;
	height: ${BLEED_SIZE + HEADER_HEIGHT}cm;
	padding: ${BLEED_SIZE + PADDING}cm ${BLEED_SIZE + PADDING}cm 0;
	background: ${({ color }) => colors.GAME[color]};

	&:before {
		content: '';
		position: absolute;
		top: 0px;
		right: 0px;
		bottom: 0px;
		left: 0px;
		background: linear-gradient(
			160deg,
			rgba(255, 255, 255, ${GRADIENT_INTENSITY}),
			rgba(0, 0, 0, ${GRADIENT_INTENSITY})
		);
	}
`;

const Content = styled.div`
	box-sizing: border-box;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;
	width: 100%;
	height: ${BLEED_SIZE + CONTENT_HEIGHT}cm;
	padding: ${PADDING}cm ${BLEED_SIZE + PADDING}cm ${BLEED_SIZE + PADDING}cm;
	background: ${colors.GAME.white};
`;

export {
	Wrapper,
	Header,
	Content,
};