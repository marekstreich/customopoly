import styled from '@emotion/styled';
import { colors } from '../../utilities/styles';
import { HEADER_TEXT_SHADOW } from './Card.constants';

const Meta = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`;

const MetaItem = styled.div`
	color: ${colors.GAME.white};
	text-transform: uppercase;
	font-size: 0.35cm;
	line-height: 0.35cm;
	font-weight: 700;
	text-shadow: ${HEADER_TEXT_SHADOW};
`;

const Name = styled.div`
	flex: 1;
	display: flex;
	justify-content: center;
	align-items: center;
	color: ${colors.GAME.white};
	text-transform: uppercase;
	font-size: 0.6cm;
	line-height: 0.6cm;
	font-weight: 400;
	text-align: center;
	text-shadow: ${HEADER_TEXT_SHADOW};
`;

const Section = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
`;

const Row = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`;

const Col = styled.div`
	color: ${colors.GAME.black};
	text-transform: uppercase;
	font-size: 0.3cm;
	line-height: 0.3cm;
	font-weight: 400;
`;

const ColWithText = styled(Col)`
	text-align: center;
`;

const Value = styled(Col)`
	font-weight: 700;
`;

const Text = styled.div`
	text-transform: uppercase;
	font-size: 0.25cm;
	line-height: 0.25cm;
	font-weight: 400;
	color: ${colors.GAME.gray};
	text-align: center;
`;

export {
	Meta,
	MetaItem,
	Name,
	Section,
	Row,
	Col,
	ColWithText,
	Value,
	Text,
};
