const CARD_WIDTH = 5.5;
const CARD_HEIGHT = 8;
const PADDING = 0.3;
const HEADER_TEXT_SHADOW = '1px 1px 0px rgba(0, 0, 0, 0.3)';

export {
	CARD_WIDTH,
	CARD_HEIGHT,
	PADDING,
	HEADER_TEXT_SHADOW,
}
