import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../utilities/styles';
import { BLEED_SIZE } from '../Bleeds/Bleeds.constants';
import { CARD_WIDTH, CARD_HEIGHT, PADDING, HEADER_TEXT_SHADOW } from './Card.constants';

const HEADER_HEIGHT = 3.75;
const CONTENT_HEIGHT = CARD_HEIGHT - HEADER_HEIGHT - 0.15;
const GRADIENT_INTENSITY = 0.15;

const Wrapper = styled.div`
	position: relative;
	width: ${BLEED_SIZE * 2 + CARD_WIDTH}cm;
	height: ${BLEED_SIZE * 2 + CARD_HEIGHT}cm;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	background: ${colors.GAME.black};
	font-family: ${fontFamilies.dosis};
`;

const Header = styled.div<{ color?: string; }>`
	position: relative;
	box-sizing: border-box;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;
	width: 100%;
	height: ${BLEED_SIZE + HEADER_HEIGHT}cm;
	padding: ${BLEED_SIZE + PADDING}cm ${BLEED_SIZE + PADDING}cm ${PADDING}cm;
	background: ${({ color = colors.GAME.gray }) => color};

	&:before {
		content: '';
		position: absolute;
		top: 0px;
		right: 0px;
		bottom: 0px;
		left: 0px;
		background: linear-gradient(
			160deg,
			rgba(255, 255, 255, ${GRADIENT_INTENSITY}),
			rgba(0, 0, 0, ${GRADIENT_INTENSITY})
		);
	}
`;

const Icon = styled.div`
	align-self: center;
	width: 1cm;
	height: 1cm;
	color: ${colors.GAME.white};
	filter: drop-shadow(${HEADER_TEXT_SHADOW});

	svg {
		width: 1cm;
		height: 1cm;
	}
`;

const Content = styled.div`
	box-sizing: border-box;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: stretch;
	width: 100%;
	height: ${BLEED_SIZE + CONTENT_HEIGHT}cm;
	padding: ${PADDING}cm ${BLEED_SIZE + PADDING}cm ${BLEED_SIZE + PADDING}cm;
	background: ${colors.GAME.white};
`;

export {
	Wrapper,
	Header,
	Icon,
	Content,
};