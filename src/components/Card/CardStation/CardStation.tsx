import React, { FunctionComponent } from 'react';
import { FaBusAlt } from 'react-icons/fa';
import { CardStationProps } from '../../../types/components/Card';
import Bleeds from '../../Bleeds/Bleeds';
import Currency from '../../Currency/Currency';
import Separator from '../Separator/Separator';
import {
	Meta,
	MetaItem,
	Name,
	Section,
	Row,
	Col,
	Value,
 } from '../CardCommon.styled';
import {
	Wrapper,
	Header,
	Icon,
	Content,
} from '../CardSpecial.styled';

const CardStation: FunctionComponent<CardStationProps> = ({
	name,
	number,
}) => {
	return (
		<Wrapper>
			<Bleeds />
			<Header>
				<Meta>
					<MetaItem>{number}</MetaItem>
				</Meta>
				<Name>{name}</Name>
				<Icon>
					<FaBusAlt />
				</Icon>
			</Header>
			<Content>
				<Section>
					<Row>
						<Col>Cena zakupu</Col>
						<Value>
							<Currency amount={400} />
						</Value>
					</Row>
				</Section>
				<Separator>Opłata za postój</Separator>
				<Section>
					<Row>
						<Col>Jeśli gracz posiada:</Col>
					</Row>
					<Row>
						<Col>1 pętlę</Col>
						<Value>
							<Currency amount={50} />
						</Value>
					</Row>
					<Row>
						<Col>2 pętle</Col>
						<Value>
							<Currency amount={100} />
						</Value>
					</Row>
					<Row>
						<Col>3 pętle</Col>
						<Value>
							<Currency amount={200} />
						</Value>
					</Row>
					<Row>
						<Col>4 pętle</Col>
						<Value>
							<Currency amount={400} />
						</Value>
					</Row>
				</Section>
				<Separator />
				<Section>
					<Row>
						<Col>Zastaw hipoteczny</Col>
						<Value>
							<Currency amount={200} />
						</Value>
					</Row>
				</Section>
			</Content>
		</Wrapper>
	);
}

export default CardStation;
