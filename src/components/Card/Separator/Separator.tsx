import React, { FunctionComponent } from 'react';
import { Wrapper, Text } from './Separator.styled';

const Separator: FunctionComponent = ({ children }) => {
	return (
		<Wrapper>
			{children && <Text>{children}</Text>}
		</Wrapper>
	);
};

export default Separator;
