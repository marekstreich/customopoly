
import styled from '@emotion/styled';
import { colors } from '../../../utilities/styles';
import { Text as CommonText } from '../CardCommon.styled';

const Wrapper = styled.div`
	position: relative;
	min-height: 2px;
	display: flex;
	justify-content: center;
	align-items: center;

	&:before {
		content: '';
		position: absolute;
		top: 50%;
		width: 100%;
		height: 2px;
		left: 0;
		margin-top: -1px;
		background: ${colors.GAME.gray};
	}
`;

const Text = styled(CommonText)`
	position: relative;
	background: ${colors.GAME.white};
	padding: 0 0.15cm;
`;

export {
	Wrapper,
	Text,
};
