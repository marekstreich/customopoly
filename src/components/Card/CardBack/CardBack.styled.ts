import styled from '@emotion/styled';
import { colors, fontFamilies } from '../../../utilities/styles';
import { BLEED_SIZE } from '../../Bleeds/Bleeds.constants';
import { CARD_WIDTH, CARD_HEIGHT, PADDING } from '../Card.constants';

const Wrapper = styled.div`
	position: relative;
	box-sizing: border-box;
	width: ${BLEED_SIZE * 2 + CARD_WIDTH}cm;
	height: ${BLEED_SIZE * 2 + CARD_HEIGHT}cm;
	padding: ${BLEED_SIZE}cm ${BLEED_SIZE + PADDING}cm;
	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
	align-items: center;
	background: ${colors.GAME.white};
	font-family: ${fontFamilies.dosis};
`;

const Text = styled.div`
	text-transform: uppercase;
	font-size: 0.3cm;
	line-height: 0.3cm;
	font-weight: 700;
	color: ${colors.GAME.gray};
	text-align: center;
`;

const Location = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const District = styled.div`
	text-transform: uppercase;
	font-size: 0.3cm;
	line-height: 0.3cm;
	font-weight: 700;
	color: ${colors.GAME.black};
	margin-bottom: 0.15cm;
`;

const Street = styled.div`
	text-transform: uppercase;
	font-size: 0.6cm;
	line-height: 0.6cm;
	font-weight: 400;
	color: ${colors.GAME.black};
	text-align: center;
`;

const Price = styled.div`
	text-transform: uppercase;
	font-size: 1.2cm;
	line-height: 1.2cm;
	font-weight: 400;
	color: ${colors.GAME.black};
`;

const Separator = styled.div`
	width: 50%;
	height: 2px;
	background: ${colors.GAME.gray};
`;

export {
	Wrapper,
	Text,
	Location,
	District,
	Street,
	Price,
	Separator,
};