import React, { FunctionComponent } from 'react';
import { CardBackProps } from '../../../types/components/Card';
import Currency from '../../Currency/Currency';
import {
	Wrapper,
	Text,
	Location,
	District,
	Street,
	Price,
	Separator,
} from './CardBack.styled';

const CardBack: FunctionComponent<CardBackProps> = ({ name, district, price }) => {
	return (
		<Wrapper>
			<Text>Karta hipoteczna</Text>
			<Separator />
			<Location>
				{district && <District>{district}</District>}
				<Street>{name}</Street>
			</Location>
			<Separator />
			<Price>
				<Currency amount={price * 0.5} />
			</Price>
			<Separator />
			<Text>Jeśli posiadłość jest zastawiona to karta musi być odwrócona</Text>
		</Wrapper>
	);
}

export default CardBack;
